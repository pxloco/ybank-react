import React, {Component} from 'react';
import {
    StyleSheet, View, Text
} from 'react-native';
import Slider from 'react-native-slider';
import ConstValue from '../consts';

export default class SliderContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {minimumValue, maximumValue, step, onValueChange,value} = this.props;
        return (
            <View style={styles.sliderContainer}>
                <Text style={styles.min} numberOfLines={1}>{minimumValue}</Text>
                <Text style={styles.max} numberOfLines={1}>{maximumValue}</Text>
                <Slider
                    trackStyle={styles.track}
                    thumbStyle={styles.thumb}
                    step={step}
                    value={value}
                    minimumValue={minimumValue}
                    maximumValue={maximumValue}
                    onValueChange={(value) => onValueChange(value)}
                />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    track: {
        height: 2,
        borderRadius: 1,
    },
    thumb: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: ConstValue.primaryColor,

    },
    sliderContainer: {
        width: ConstValue.widthScreen - 8 * ConstValue.margin,
        marginLeft: 2 * ConstValue.margin,
        paddingBottom: 2 * ConstValue.padding,
        marginRight: 2 * ConstValue.margin,
        marginTop: 2 * ConstValue.margin
    },
    min: {
        position: 'absolute',
        top: -5,
        left: 0,
        fontSize: ConstValue.smallText.fontSize
    },
    max: {
        position: 'absolute',
        top: -5,
        right: 0,
        fontSize: ConstValue.smallText.fontSize
    }
});