/**
 * Created by pxloc on 05-Jul-17.
 */
import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import ConstValue from '../consts';
import Button from 'apsl-react-native-button';

export default class ButtonCommon extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {onPress, text, style, textStyle,isLoading} = this.props;
        return (
                <Button style={[styles.layout_button,style]}
                        isLoading={isLoading}
                        textStyle={textStyle}
                        onPress={() => onPress()}>
                    {text}
                </Button>
        );
    }
}

const styles = StyleSheet.create({
    layout_button: {
        width: ConstValue.widthScreen / 2 - 40,
        margin: 10,
        height: 40
    },
});