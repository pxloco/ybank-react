import React, {Component} from 'react';
import Icon from "react-native-vector-icons/FontAwesome";
import {
    View,
    StyleSheet,
    Text,
    TextInput, TouchableHighlight
} from 'react-native';

import ConstValue from '../consts';

export default class SecureInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secureText: true,
        }
    }

    onToggleShowPassword() {
        this.setState(previousState => {
                return {secureText: !previousState.secureText}
            }
        );
    }

    render() {
        let {label, onChangeText, value = null, editable = true, hint} = this.props;
        return (
            <View style={styles.input_container}>
                {label ? <Text style={styles.label}>{label}</Text> : null}
                <TextInput secureTextEntry={this.state.secureText}
                           underlineColorAndroid={'transparent'}
                           placeholder={hint}
                           style={styles.text_input}
                           onChangeText={(value) => onChangeText(value)}
                />
                <TouchableHighlight
                    style={styles.button_icon}
                    onPress={() => this.onToggleShowPassword()}
                    underlayColor="transparent">
                    <Icon style={styles.password_icon} size={18}
                          name={this.state.secureText ? "eye" : "eye-slash"}/>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_input: {
        fontSize: ConstValue.inputText.fontSize,
        backgroundColor: "white",
        borderWidth: 1.5,
        borderColor: '#068AD5',
        textAlign: 'left',
        height: 35,
        width: ConstValue.widthScreen - 4 * ConstValue.padding,
        borderRadius: 5,
        paddingLeft: ConstValue.padding
    },
    input_container: {
        marginTop: 8,
        marginBottom: 16
    },
    label: {
        fontSize: ConstValue.h2.fontSize,
        fontWeight: ConstValue.h2.fontWeight,
        marginBottom: 8
    },
    button_icon: {
        position: 'absolute',
        right: 10,
        top: 8,
        backgroundColor: 'transparent',
    }
});