import React, {Component} from 'react';

import {
    View,
    StyleSheet,
    Text
} from 'react-native';

import RadioForm from './react-native-simple-radio-button';

import ConstValue from '../consts';

export default class Radio extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {label, onPress, values,initial = 0} = this.props;
        return (
            <View style={styles.input_container}>
                {label?<Text style={styles.label}>{label}</Text>: null}
                <RadioForm
                    formHorizontal={true}
                    animation={true}
                    radio_props={values}
                    initial={initial}
                    borderWidth={2}
                    radioStyle={{flex:1}}
                    labelStyle={{margin: 8, marginRight:12, marginLeft: 0, fontSize: ConstValue.normalText.fontSize}}
                    onPress={(value) => onPress(value)}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input_container: {
    },
    label: {
        fontSize: ConstValue.h2.fontSize,
        fontWeight: ConstValue.h2.fontWeight,
    },
});