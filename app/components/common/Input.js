import React, {Component} from 'react';

import {
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native';

import ConstValue from '../consts';

export default class Input extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        let {label, value = null} = this.props;
        return (
            <View style={styles.input_container}>
                {label?<Text style={styles.label}>{label}</Text>:null}
                <TextInput
                    value={value}
                    {...this.props}
                    underlineColorAndroid={'transparent'}
                    style={styles.text_input}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text_input: {
        fontSize: ConstValue.inputText.fontSize,
        backgroundColor: "white",
        borderWidth: 1.5,
        borderColor: '#068AD5',
        textAlign: 'left',
        height: 35,
        width: ConstValue.widthScreen - 4 * ConstValue.padding,
        borderRadius: 5,
        paddingLeft: ConstValue.padding
    },
    input_container: {
        marginTop: 8,
        marginBottom: 16
    },
    label: {
        fontSize: ConstValue.h2.fontSize,
        fontWeight: ConstValue.h2.fontWeight,
        marginBottom: 8
    },
});