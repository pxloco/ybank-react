import React, {Component} from 'react';
import Bullet from './Bullet';
import {
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native';


import ConstValue from '../consts';

export default class Legend extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        let {number, title, type=1} = this.props;
        return (
            <View style={type == 1? styles.legend: styles.legend_small}>
                <Bullet type={type} number={number}/>
                <Text style={type == 1?styles.title :styles.title_small}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    legend: {
        flexDirection:'row',
        paddingBottom: ConstValue.padding * 2,
        marginTop: ConstValue.margin * 2
    },
    title: {
        fontSize: ConstValue.titleText.fontSize,
        fontWeight: ConstValue.titleText.fontWeight,
        marginLeft: ConstValue.margin,
        marginTop: ConstValue.margin/2,
    },
    legend_small: {
        flexDirection:'row',
        paddingBottom: ConstValue.padding * 2
    },
    title_small: {
        fontSize: ConstValue.titleText.fontSize - 3,
        fontWeight: ConstValue.titleText.fontWeight,
        marginLeft: ConstValue.margin,
        marginTop: ConstValue.margin/2,
    }
});