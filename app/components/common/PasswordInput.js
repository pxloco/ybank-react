import React, {Component} from 'react';
import {
    View,
    TouchableHighlight,
    StyleSheet,
    TextInput,
    Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ConstValue from '../consts';

export default class PasswordInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secureText: true,
        }
    }

    onToggleShowPassword() {
        this.setState(previousState => {
                return {secureText: !previousState.secureText}
            }
        );
    }

    onComponent

    render() {
        return (
            <View style={styles.container_input}>
                <TextInput secureTextEntry={this.state.secureText}
                           underlineColorAndroid={'transparent'}
                           placeholder={language.matkhau}
                           style={styles.text_input}
                           onChangeText={(value) => onChangeText(value)}
                           {...this.props}
                />
                <TouchableHighlight
                    style={styles.button_icon}
                    onPress={() => this.onToggleShowPassword()}
                    underlayColor="transparent">
                    <Icon style={styles.password_icon} size={18}
                          name={this.state.secureText ? "eye" : "eye-slash"}/>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container_input: {
        width: ConstValue.widthScreen - 40,
        height: 50,
        marginTop: 20,
        borderRadius: 10,
        backgroundColor: ConstValue.primaryColorInverse,
        flexDirection: 'row'
    },
    text_input: {
        fontSize: 16,
        flex: 1,
        textAlign: 'left',
        borderRadius: 6,
        paddingLeft: 8,
        backgroundColor: 'white'
    },
    button_icon: {
        position: 'absolute',
        right: 10,
        top: 15,
        backgroundColor: 'transparent',
    }
});

