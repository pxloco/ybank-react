/**
 * Created by vhnvn on 7/2/17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    AppRegistry,
    StyleSheet, Button,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch
} from 'react-native';

import * as globalActions from '../../actions/global';

import consts from '../consts';

class HomeHeader extends Component {
    //region data mutators

    //endregion
    render () {
        return (
            <View style={styles.container}>
                <TouchableHighlight style={styles.btn_online_support}>
                    <View style={styles.txt_online_support_container}>
                        <Text style={styles.txt_online_support}>Hỗ trợ online</Text>
                    </View>
                </TouchableHighlight>
                <View style={styles.navigation_title}>
                    <Text style={styles.txt_navigation_title}>YBank</Text>
                </View>
                <View style={styles.switch_container}>
                    <Switch
                        style={styles.switch}
                        onTintColor="#02507E"
                        thumbTintColor="white"
                        tintColor="#02507E"
                        onValueChange={(val) => this.props.toggleTopSwitch({topSwitchValue: val})}
                        value={ this.props.topSwitchValue }/>
                </View>
            </View>
        );
    }
}
;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: consts.appBarHeight,
        paddingTop:consts.statusBarHeight,
        backgroundColor: consts.primaryColor,
    },

    btn_online_support: {
        marginLeft: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },

    txt_online_support_container: {
        borderRadius: 30,
        padding: 5,
        marginLeft: 5,
        paddingLeft: 10,
        backgroundColor: consts.primaryColorInverse
    },

    txt_online_support: {
        fontSize: consts.normalText.fontSize,
        fontWeight: '500',
        color: consts.primaryColor,
    },

    navigation_title: {
        backgroundColor: consts.primaryColor,
        justifyContent: 'center',
    },

    txt_navigation_title: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        marginRight: 30,
    },

    switch_container: {
        backgroundColor: consts.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 16
    },

    switch: {
        marginRight: -10,
        transform: [
            {scale: 0.7,}
        ]
    },
});

function select (store) {
    return {
        topSwitchValue: store.global.topSwitchValue
    }

}

export default connect(select, {
    ...globalActions
})(HomeHeader);