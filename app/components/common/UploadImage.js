import React, {Component} from 'react';

import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ActivityIndicator
} from 'react-native';

import ConstValue from '../consts';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import api from '../../api/api';

let options = {
    title: 'Chọn hình',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

export default class UploadImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        }
    }

    onPickFile() {
        let {onImageChange} = this.props;
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
            }
            else {
                this.setState({isLoading: true});
                let source = {uri: response.uri};
                ImageResizer.createResizedImage(response.uri, 800, 600, 'JPEG', 80)
                    .then((resizedImageUri) => {
                        api.upload_file({uri: resizedImageUri, filename: response.filename}).then(
                            url => {
                                onImageChange(url);
                                this.setState({isLoading: false});
                            },
                            message => {
                                this.setState({isLoading: false});
                                if (message)
                                    Alert.alert(message);
                            }
                        );
                    });
            }
        });
    }

    render() {
        let {url, style, is_avatar, disabled} = this.props;
        return (
            this.state.isLoading ?<ActivityIndicator/>:
                <TouchableOpacity disabled={disabled} style={[style]} onPress={() => this.onPickFile()}>
                    {url ?
                        <Image style={is_avatar ? styles.avatarImage : styles.image}
                               source={{uri: url}}/> :
                        <Image style={styles.avatarImage}
                               source={require('../img/Icon_AnhDaI Dien@3x.png')}/>}

                </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 200,
    },
    avatarImage: {
        width: ConstValue.image.width,
        height: ConstValue.image.height,
        borderRadius: ConstValue.image.height / 2,
        padding: ConstValue.padding,
    },
});