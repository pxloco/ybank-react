import React, {Component} from 'react';
import ButtonAP from 'apsl-react-native-button';

import {
    View,
    StyleSheet,
} from 'react-native';

import ConstValue from '../consts';

export default class Button extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        let {onPress, isLoading, text} = this.props;
        return (
            <View style={styles.layout_button}>
                <ButtonAP style={styles.button}
                          textStyle={styles.buttonText}
                          containerStyle={{padding: 0}}
                          isLoading={isLoading}
                          onPress={() => onPress()}>
                    {text}
                </ButtonAP>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    layout_button: {
        width: 120,
        marginBottom: 100,
        marginTop: 30
    },
    button: {
        backgroundColor: ConstValue.primaryColor,
        borderColor: ConstValue.primaryColor,
        height: 30,
        borderRadius: 4,
    },
    buttonText: {
        fontSize: ConstValue.normalText.fontSize,
        color: 'white',
        fontWeight: 'bold'
    },
});