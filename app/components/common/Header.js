import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    AppRegistry,
    StyleSheet, Button,
    Text, Image, View,
} from 'react-native';


import ConstValue from '../consts';

export default class Header extends Component {

    render() {
        let {title, rightView} = this.props;
        return (
            <View style={styles.navigation_bar}>
                <View style={styles.navigation_bar_content}>
                    <Text style={styles.title}>{title}</Text>
                    {rightView ? (<View style={styles.right_area}>
                        {rightView}
                    </View>) : null}
                </View>
            </View>
        );
    }
};


const styles = StyleSheet.create({
    navigation_bar: {
        flexDirection: 'row',
        height: ConstValue.appBarHeight,
        paddingTop: ConstValue.statusBarHeight,
        backgroundColor: ConstValue.primaryColor
    },
    navigation_bar_content: {
        flex:1,
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'row',
        alignSelf: 'center'
    },
    title: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    right_area: {
        position: 'absolute',
        right: 0, bottom: 0, top: 0
    }
});