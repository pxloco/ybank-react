import React, {Component} from 'react';

import {
    View,
    StyleSheet,
    Text
} from 'react-native';

import ConstValue from '../consts';

export default class Bullet extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {number, type=1} = this.props;
        return (
            <View style={type == 1 ?styles.bullet:styles.bullet_small}>
                <Text style={type == 1? styles.bullet_text : styles.bullet_text_small}>{number}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bullet_text: {
        textAlign: 'center',
        color: 'white',
        fontSize: ConstValue.titleBullet.fontSize,
        fontWeight: ConstValue.titleBullet.fontWeight,
        backgroundColor: 'transparent',
    },
    bullet: {
        backgroundColor: ConstValue.primaryColor,
        borderRadius: 50,
        width: ConstValue.titleBullet.width,
        height: ConstValue.titleBullet.height,
    },

    bullet_text_small: {
        textAlign: 'center',
        color: 'white',
        fontSize: ConstValue.titleBullet.fontSize - 8,
        fontWeight: ConstValue.titleBullet.fontWeight,
        backgroundColor: 'transparent',
        paddingTop: 3
    },
    bullet_small: {
        backgroundColor: ConstValue.primaryColor,
        borderRadius: 50,
        width: ConstValue.titleBullet.width - 3,
        height: ConstValue.titleBullet.height - 3,
        marginLeft:ConstValue.margin
    },
});