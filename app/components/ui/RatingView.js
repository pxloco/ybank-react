import React, {Component} from 'react';

import {
    AppRegistry,
    StyleSheet, Button, Modal,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

import consts from '../consts';

import api from '../../api/api';


export default class ProfilePopup extends Component {
    _updateValue (newValue) {
        this.setState({value: newValue});
    }

    render () {
        const {max, value} = this.props;
        return (
            <View style={styles.container}>
                {[...Array(max)].map((x, i) => (
                    <TouchableHighlight key={`star-${i}`}
                                        onPress={ this.props.editable ? (() => this._updateValue(i + 1)) : null }
                                        activeOpacity={0.5}
                                        underlayColor={'transparent'}>
                        <Image key={`star-${i}-image`} style={styles.star}
                               source={ i < value ? require('../img/select_star@2x.png') : require('../img/unselect_star@2x.png')}/>
                    </TouchableHighlight>
                ))}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
    },
    star: {
        width: 16,
        height: 16,
        margin: 4,
    },
});