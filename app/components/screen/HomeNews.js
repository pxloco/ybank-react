import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView
} from 'react-native';

import {connect} from 'react-redux';
import {TabNavigator} from 'react-navigation';
import ImageSlider from 'react-native-image-slider';
import language from '../lang';
import ConstValue from '../consts';
import ButtonLogin from '../common/ButtonCommon';
import Header from '../common/Header';

class HomeNews extends Component {
    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../img/Gray_Icon_Navigation_Tin tuc@3x.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    constructor(props) {
        super(props);
        this.funcNavigateRegisterScreen = this.funcNavigateRegisterScreen.bind(this)
        this.funcNavigateLoginScreen = this.funcNavigateLoginScreen.bind(this)
    }

    funcNavigateRegisterScreen() {
        let {navigate} = this.props.navigation;
        navigate('HomeMap');
    }

    funcNavigateLoginScreen() {
        let {navigate} = this.props.screenProps.navigation;
        navigate('LoginScreen');
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;
        let LoginArea = null;

        LoginArea = (
            <View style={styles.chon_doi_tuong}>
                {/*<Text style={styles.info_grey}>{language.ban_thuoc_doi_tuong_nao.toUpperCase()}</Text>*/}
                <View style={styles.layout_input}>

                    <ButtonLogin
                        style={{backgroundColor: ConstValue.primaryColor, borderColor: ConstValue.primaryColor}}
                        textStyle={{color: 'white', fontSize: 16, fontWeight: 'bold'}}
                        text={language.login.toUpperCase()}
                        onPress={this.funcNavigateLoginScreen}
                    />

                    <ButtonLogin
                        style={{backgroundColor: 'white', borderColor: ConstValue.primaryColor}}
                        textStyle={{color: ConstValue.primaryColor, fontSize: 16, fontWeight: 'bold'}}
                        text={language.register.toUpperCase()}
                        onPress={this.funcNavigateRegisterScreen}
                    />
                </View>
            </View>
        );

        return (
            <View style={styles.container}>
                <Header title={"Tin tức"}/>
                <View style={styles.container_body}>
                    <View style={styles.layout_hinh_anh_gioi_thieu}>
                        <Image style={styles.hinh_anh_gioi_thieu}
                               resizeMode='stretch'
                               source={require('../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}/>
                    </View>
                    <View style={styles.image_slider}>
                        <ImageSlider
                            style={styles.slider1}
                            images={[
                                'http://placeimg.com/640/480/any',
                                'http://placeimg.com/640/481/any',
                                'http://placeimg.com/640/482/any',
                            ]}/>
                    </View>

                    <View style={styles.tin_tuc}>
                        <Text style={styles.tieu_de_tin_tuc}>LÃI SUẤT CHỈ TỪ 7,3% năm</Text>
                        <Text style={styles.noidung}>Với mục đích hỗ trợ khách hàng thực hiện những kế hoạch tiêu
                            dùng...</Text>
                        <TouchableOpacity onPress={() => {
                            navigate('ChatScreen')
                        }}>
                            <Text style={styles.xemthem}>Xem thêm >>></Text>
                        </TouchableOpacity>
                    </View>

                    {this.props.user.is_logged_in && this.props.user.status == 3 ? null : LoginArea}
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    container_body: {
        flex: 1,
    },
    image_slider: {
        marginBottom: 8,
    },
    chon_doi_tuong: {
        height: 60,
        alignItems: 'center',
    },
    info_grey: {
        fontSize: ConstValue.titleText.fontSize + 2,
        fontWeight: '500',
        marginTop: 16,
        marginBottom: 8,
        color: ConstValue.silverColor
    },
    layout_input: {
        flex: 1,
        flexDirection: 'row'
    },
    layout_button: {
        width: 150,
        height: 50,
        margin: 20,
    },

    can_chieu_ngang: {
        flexDirection: 'row',
        padding: 5,

    },
    can_chieu_doc: {
        flexDirection: 'column',
    },
    navigation_bar: {
        flexDirection: 'row',
        height: ConstValue.appBarHeight,
        paddingTop: ConstValue.statusBarHeight,
        backgroundColor: ConstValue.primaryColor
    },
    navigation_bar_content: {
        flex: 1,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    hinh_anh_gioi_thieu: {
        flex: 1,
        width: ConstValue.widthContent
    },
    layout_hinh_anh_gioi_thieu: {
        flex: 1,
        margin: 8,
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    slider1: {
        margin: 8,
        marginTop: 0
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    tieu_de_tin_tuc: {
        color: 'black',
        fontSize: 17,
        fontWeight: '500',
        backgroundColor: 'white',
    },
    noidung: {
        paddingTop: 8,
        fontSize: 15,
        color: ConstValue.silverColor,
        paddingBottom: 8
    },
    xemthem: {
        color: '#068AD5',
        fontSize: 14,

    },
    tin_tuc: {
        flex: 1,
        margin: 8,
        padding: 16,
        backgroundColor: 'white',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#e7e7e7'
    },
    icon: {
        width: 25,
        height: 25
    }
});

function select(store) {
    return {
        user: store.user
    }
}

export default connect(select)(HomeNews);