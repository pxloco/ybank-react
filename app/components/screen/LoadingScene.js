import React, {Component} from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

import consts from '../consts';
import * as loginActions from '../../actions/login';

class LoadingScene extends Component {
    constructor(...args) {
        super(...args);
    }

    componentDidMount() {
        this.checkAuthentication();
    }

    componentWillUpdate(nextProps, nextState) {
        let {user} = nextProps;

        this.authenticatingVerificationCompleted(user);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.content}>Đang cập nhật...</Text>
            </View>
        );
    }

    checkAuthentication() {
        try {
            this.reloginUser();
        } catch (e) {
            this.logout();
        }

    }

    async reloginUser() {
        let {user} = this.props;

        if (user && user.token) {
            try {
                await this.props.relogin(user.token);
            } catch (e) {
                this.logout();
            }

            return;
        }

        this.authenticatingVerificationCompleted(user);
    }

    logout() {
        this.props.sign_out();
    }

    authenticatingVerificationCompleted(user) {
        let {navigation} = this.props;
        let {navigate} = navigation;

        if (user && user.status == 2) {
            let goToUpdateProfileScreenAction = NavigationActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'LoginTabScenes'
                    }),
                    NavigationActions.navigate({
                        routeName: this.props.user.is_lender ? 'UpdateProfileLender' : 'UpdateProfileBorrower'
                    })
                ]
            });
            navigation.dispatch(goToUpdateProfileScreenAction);
        } else {
            let goToLoginTabScenesAction = NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'LoginTabScenes'
                    })
                ]
            });
            navigation.dispatch(goToLoginTabScenesAction);;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: consts.primaryColor,
    },
    content: {
        color: consts.primaryColorInverse,
    },
});

function select(store) {
    return {
        user: store.user
    }
}

function actions(dispatch) {
    return {
        relogin: (token) => dispatch(loginActions.login_from_server({token})),
        sign_out: () => dispatch(loginActions.sign_out())
    }
}

export default connect(select, actions)(LoadingScene);