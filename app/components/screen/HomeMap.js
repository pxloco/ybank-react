import React, {Component} from 'react';

import HomeMapLoggedIn from './MapScreen/HomeMapLoggedIn';
import HomeMapNotLoggedIn from './MapScreen/MapScreenNotLogin';

import {connect} from 'react-redux';

class HomeMap extends Component {
    render () {
        return this.props.user.is_logged_in && this.props.user.status == 3 ? <HomeMapLoggedIn {...this.props}/> : <HomeMapNotLoggedIn {...this.props}/>
    }
}

function select (store) {
    return {
        user: store.user
    }
}

export default connect(select)(HomeMap);