/**
 * Created by pxloc on 27-Jun-17.
 */
import React, {Component} from 'React';
import {AppRegistry, StyleSheet, View, Text, TextInput, Alert} from 'react-native';
import Button from '../common/Button';
import PasswordInput from '../common/SecureInput';
import ConstValue from '../consts';
import language from '../lang';

export default class UpdatePassword extends Component {
    static navigationOptions = {
        title: "Cài đặt tài khoản",
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    constructor(props) {
        super(props);
        this.state = {
            passwordOld: null,
            passwordNew: null,
            passwordNewAgain: null,
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.component_container}>

                    <Text style={styles.text_tieu_de}>Mật khẩu hiện tại</Text>

                    <PasswordInput
                        onChangeText={(passwordOld) => this.setState({passwordOld})}
                        hint={language.matkhau}
                    />
                    <Text style={styles.text_tieu_de}>Mật khẩu mới</Text>
                    <PasswordInput
                        onChangeText={(passwordNew) => this.setState({passwordNew})}
                        hint={language.matkhaumoi}
                    />
                    <Text style={styles.text_tieu_de}>Nhập lại mật khẩu</Text>
                    <PasswordInput
                        onChangeText={(passwordNewAgain) => this.setState({passwordNewAgain})}
                        hint={language.nhaplaimatkhau}
                    />
                    <View style={styles.can_giua}>
                        <Button
                            text={language.capnhat}
                            onPress={() => {
                                navigate('ConfirmPINCodeRegisterScreen')
                            }}/>
                    </View>
                </View>
            </View>
        );
    }
}

const onDangKy = () => {
    Alert.alert('Cập nhật');
}

const styles = StyleSheet.create({
        text_tieu_de: {
            fontSize: 16,
            color: 'black',
            fontWeight: 'bold',
            marginTop: 8,
        },
        can_giua: {
            alignItems: 'center',
        },
        container: {
            flex: 1,
            flexDirection: 'column',
            backgroundColor: 'white',
        },
        component_container: {
            marginLeft: 20,
            marginRight: 20,
            marginTop: 8,
        },
        blue_input: {
            backgroundColor: 'white',
            width: ConstValue.widthScreen - 32,
            height: 50,
            borderRadius: 10,
            padding: 10,
            fontSize: 16,
            marginLeft: 10,
            marginTop: 5,
            marginBottom: 10,
            borderColor: '#068AD5',
            borderWidth: 2,
        },
        title_xacnhanmatkhau: {
            color: 'black',
            fontSize: 18,
            textAlign: 'center',
            marginTop: 20,
            marginBottom: 20,
            marginLeft: 40,
            marginRight: 40,
        },
    }
);