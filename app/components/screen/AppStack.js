import {StackNavigator} from 'react-navigation';
import LoadingScene from './LoadingScene';
import LoginTabScenes from './LoginTabScenes';
import RegisterScreen from './RegisterScreen';
import ConfirmPINCodeRegisterScreen from './ConfirmPINCodeRegisterScreen';
import UpdateProfileLender from '../scenes/UpdateProfileLender';
import UpdateProfileBorrower from '../scenes/UpdateProfileBorrower';
import ChatScreen from './ChatScreen';
import LoginScreen from './LoginScreen';
import ConstValue from '../consts';
import InformationSettingScreen from './InformationSettingScreen';
import PrivateSettingsScreen from './PrivateSettingsScreen';
import UpdatePassword from './UpdatePassword';
import language from '../lang';

export default LoginStack = StackNavigator({
    LoadingScene: {
        screen: LoadingScene,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LoginTabScenes: {
        screen: LoginTabScenes,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    RegisterScreen: {
        screen: RegisterScreen,
        navigationOptions: ({navigation}) => ({
            title: null,
        })
    },
    ConfirmPINCodeRegisterScreen: {
        screen: ConfirmPINCodeRegisterScreen,
        navigationOptions: ({navigation}) => ({
            title: 'Nhập Mã Code'
        })
    },
    UpdateProfileLender: {
        screen: UpdateProfileLender,
        navigationOptions: ({navigation}) => ({
            title: language.cap_nhat_thong_tin
        })
    },
    UpdateProfileBorrower: {
        screen: UpdateProfileBorrower,
        navigationOptions: ({navigation}) => ({
            title: language.cap_nhat_thong_tin
        })
    },
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: ({navigation}) => ({
            title: null
        })
    },
    chat: {
        path: 'chat/:username',
        screen: ChatScreen,
        navigationOptions: ({navigation}) => ({
            title: `${navigation.state.params.user.full_name}`
        })
    },
    InformationSettingScreen: {
        screen: InformationSettingScreen,
        navigationOptions: ({navigation}) => ({
            title: 'Cài đặt thông tin'
        })
    },
    PrivateSettingsScreen: {
        screen: PrivateSettingsScreen,
        navigationOptions: ({navigation})  => ({
            title: 'Cài đặt riêng tư'
        })
    },
    UpdatePassword: {
        screen: UpdatePassword,
        navigationOptions: ({navigation})  => ({
            title: 'Cài đặt tài khoản'
        })
    }
}, {
    initialRouteName: 'LoadingScene',
    navigationOptions: {
        headerTintColor: ConstValue.primaryColorInverse,
        headerStyle: {
            backgroundColor: ConstValue.primaryColor,
        }
    }
});
