import React, {Component} from 'react';

import {Image, StyleSheet} from 'react-native';
import ProfileLogin from './Profile/ProfileLogin';
import ProfileDetailLender from './Profile/ProfileDetailLender';
import ProfileDetailBorrower from './Profile/ProfileDetailBorrower';

import {connect} from 'react-redux';

class Profile extends Component {

    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../img/gray_icon_navigation_trangcanhan@3x.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    render () {
        return this.props.user.is_logged_in && this.props.user.status ? this.props.user.is_lender?<ProfileDetailLender {...this.props}/>:
            <ProfileDetailBorrower {...this.props}/> : <ProfileLogin {...this.props}/>
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    icon: {
        width: 25,
        height: 25
    }
});

function select (store) {
    return {
        user: store.user
    }
}

export default connect(select)(Profile);