import React, {Component} from 'react';

import {
    AppRegistry,
    StyleSheet, Button,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch
} from 'react-native';
import consts from '../consts';
import api from '../../api/api';
import {GiftedChat, LoadEarlier} from 'react-native-gifted-chat';

import Icon from 'react-native-vector-icons/FontAwesome';

import {connect} from 'react-redux';

import chat from '../../chat/chat';

class ChatScreen extends Component {
    constructor () {
        super();
        this.state = {
            canLoadEarlier: true,
            isLoadingEarlier: false,
            messages: []
        };
    }

    onSend (messages = []) {
        let {navigation} = this.props;
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            };
        });
        messages.forEach((m) => chat.sendMessage('message', {to: navigation.state.params.user.username, ...m}));
    }

    loadEarlier () {
        if (!this.state.isLoadingEarlier) {
            this.setState({isLoadingEarlier: true});
            let {messages} = this.state;
            if (messages.length) {
                console.log('load-before', messages[messages.length - 1]);
                chat.loadEarlier(this.props.navigation.state.params.user.username, messages[messages.length - 1].id);
            } else {
                chat.loadEarlier(this.props.navigation.state.params.user.username, null);
            }
            setTimeout(() => {
                this.setState({isLoadingEarlier: false});
            }, 1000);
        }
    }

    componentDidMount () {
        let {navigation} = this.props;
        console.log('user', navigation.state.params.user);
        this.message_update_dispose = chat.registerMessagesUpdate(navigation.state.params.user.id, (type, ...params) => this.messagesUpdate(type, ...params));
        chat.updateMessages(navigation.state.params.user.username);
    }

    componentWillUnmount () {
        if (this.message_update_dispose) {
            this.message_update_dispose.dispose();
        }
    }

    messagesUpdate (type, ...params) {
        let {user} = this.props;
        if (type === 'message') {
            const [new_messages] = params;
            let message = {...new_messages};
            if (!('createdAt' in message)) {
                message['createdAt'] = (function parse_datetime (datetime) {
                    let [y, m, d, h, i, s] = datetime.split(/[^0-9]+/g);
                    return new Date(Date.UTC(y, m - 1, d, h, i, s));
                })(message['created_at']);
            }
            console.log('new message: ', message);
            this.setState((previousState) => {
                let messages = [...previousState.messages];
                let found_match = false;
                let insertPosition = messages.length;
                for (let i = messages.length - 1; i >= 0; i--) {
                    if (messages[i]._id == message.body._id) {
                        console.log('update existing message');
                        found_match = true;
                        messages[i] = {id: message.id, ...messages[i], ...message};
                        break;
                    } else if (messages[i].id < message.id) {
                        insertPosition = i;
                    }
                }
                if (!found_match) {
                    let new_message = {
                        id: message.id,
                        ...message.body,
                        createdAt: message.createdAt,
                        user: this.userModelForId(message.from_user_id)
                    };
                    console.log('new_message::: ', new_message);
                    messages.splice(insertPosition, 0, new_message);
                }
                return {
                    messages
                }
            });
        } else if (type === 'cursor_close') {
            const [dir] = params;
            console.log('cursor_close', dir);
            if (dir === 'before' || this.state.messages.length < 10) {
                this.setState({canLoadEarlier: false})
            }
        }
    }

    userModelForId (id) {
        let name, avatar;
        if (id == this.props.user.user.id) {
            name = this.props.user.user.full_name;
            avatar = this.props.user.user.avatar;
        }
        if (id == this.props.navigation.state.params.user.id) {
            name = this.props.navigation.state.params.user.full_name;
            avatar = this.props.navigation.state.params.user.avatar;
        }
        return {
            _id: id,
            name,
            avatar: avatar
        }
    }

    render () {
        return (<GiftedChat
            messages={this.state.messages}
            onSend={(m) => this.onSend(m)}
            containerStyle={StyleSheet.flatten([styles.chat_container])}
            textInputStyle={StyleSheet.flatten([styles.text_input])}
            loadEarlier={ this.state.canLoadEarlier }
            onLoadEarlier={() => this.loadEarlier()}
            isLoadingEarlier={this.state.isLoadingEarlier}
            renderLoadEarlier={ (props) => (<LoadEarlier {...props} {...{label: 'Tải thêm'}}/>) }
            listViewProps={
                {
                    style: styles.messages_container
                }
            }
            renderAvatarOnTop={true}
            renderActions={ () => {
                return (<View style={styles.buttons_container}>
                    <Icon name="plus" size={20} color={consts.primaryColor}/>
                </View>);
            }}
            user={{
                _id: this.props.user.user.id,
            }}
        />);
    }
}

const styles = StyleSheet.create({
    messages_container: {
        backgroundColor: 'white',
    },
    chat_container: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        paddingLeft: 10,
    },
    text_input: {
        backgroundColor: 'white',
        borderRadius: 10,
    },
    buttons_container: {
        alignSelf: 'center'
    },
});

function select (store) {
    return {
        user: store.user,
    }
}

export default connect(select)(ChatScreen);