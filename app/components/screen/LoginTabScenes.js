import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Image
} from 'react-native';
import {TabNavigator, TabBarBottom} from 'react-navigation'
import Profile from './Profile';
import HomeNews from './HomeNews';
import MapScreen from './HomeMap';
import language from '../lang';
import {connect} from 'react-redux';
import ConstValue from '../consts';

class LoginTabScreen extends Component {
    static navigationOptions = {
        header: null,
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: ConstValue}
    }

    constructor(props) {
        super(props);

    }

    render() {
        let {navigation} = this.props;
        return (
            <MyApp screenProps={{navigation: navigation}}/>
        );
    }
}

const MyApp = TabNavigator({
    HomeMap: {
        screen: MapScreen,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('../img/Gray_Icon_Navigation_Map@3x.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                />
            ),
        }
    },
    HomeNews: {screen: HomeNews},
    Profile: {screen: Profile},
}, {
    tabBarPosition: 'bottom',
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
        showLabel: false,
        showIcon: true,
        activeTintColor: ConstValue.primaryColor,
    },
    initialRouteName: 'HomeNews'
});

const styles = StyleSheet.create({

});

function select(store) {
    return {
        user: store.user
    }
}

export default connect(select)(LoginTabScreen);