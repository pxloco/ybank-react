import React, {Component} from 'React';
import {AppRegistry, View, Text, StyleSheet, TouchableOpacity, Alert, KeyboardAvoidingView} from 'react-native';
import CodePin from '../common/PinCode';
import Button from '../common/ButtonCommon';
import language from '../lang';
import {connect} from 'react-redux';
import * as registerActions from '../../actions/register';
import {NavigationActions} from 'react-navigation'

class ComfirmPINCodeRegisterScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            pin: '',
            isLoading: false
        }

        this.onComplete = this.onComplete.bind(this);
    }

    onComplete(value) {
        this.setState({
            pin: value,
        });
    }

    static navigationOptions = {
        title: language.nhapmaxacnhan,
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'},
    }

    async onSubmit() {
        if(!this.state.pin) {
            Alert.alert('Vui lòng nhập mã pin!');
            return;
        }
        this.setState({
            isLoading: true
        });

        try {
            await this.props.verify_register_code({
                phone_number: this.props.user.phone_number,
                verified_code: this.state.pin
            });
            let goToUpdateProfileScreenAction = NavigationActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'LoginTabScenes'
                    }),
                    NavigationActions.navigate({
                        routeName: this.props.user.is_lender ? 'UpdateProfileLender' : 'UpdateProfileBorrower'
                    })
                ]
            });
            this.props.navigation.dispatch(goToUpdateProfileScreenAction);
        } catch (error) {
            console.log(error);
            Alert.alert(error);
        } finally {
            this.setState({
                isLoading: false
            });
        }
    }

    async resent_code() {
        try {
            let message = await this.props.resent_code({
                phone_number: this.props.user.phone_number
            });
            if (message)
                Alert.alert(message);
        } catch (message) {
            if (message)
                Alert.alert(message);
        }

    }

    render() {
        return (
            <View style={styles.container}>
                {/*<Text style={styles.information}>{language.chungtoidaguichobanmacode}</Text>*/}
                <KeyboardAvoidingView
                    keyboardVerticalOffset={-30}
                    behavior={'position'}>
                    <CodePin
                        ref={ref => this.ref = ref}
                        text={language.chungtoidaguichobanmacode}
                        number={4}
                        onComplete={this.onComplete}
                        pinStyle={styles.pinStyle}
                        textStyle={styles.pinTextStyle}
                        containerPinStyle={styles.containerPinStyle}
                        containerStyle={styles.containerStyle}
                        error={language.maxacnhankhongchinhxac}
                        keyboardType="numeric"
                        placeholder={"\u26AB"}
                        placeholderTextColor="black"

                    />
                </KeyboardAvoidingView>
                <View style={styles.layout_horizontal}>
                    <Text style={styles.information2}>{language.chuanhanduoccode}</Text>
                    <TouchableOpacity onPress={() => this.resent_code()}>
                        <Text style={styles.link_button}>{language.guilaicode}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.layout_button}>
                    <Button
                        style={{backgroundColor: '#068AD5', borderColor: '#068AD5'}}
                        textStyle={{fontSize: 18, color: 'white', fontWeight: 'bold'}}
                        containerStyle={{padding: 0}}
                        isLoading={this.state.isLoading}
                        text={language.hoanthanh}
                        onPress={() => this.onSubmit()}>

                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "white",
    },
    layout_horizontal: {
        flexDirection: 'row',
    },
    link_button: {
        color: '#068AD5',
        fontSize: 15,
        fontWeight: '500'
    },
    information: {
        fontSize: 16,
        color: 'black',
        margin: 20,
        textAlign: 'center',
    },
    information2: {
        fontSize: 15,
        color: 'black',
    },
    layout_button: {
        width: 150,
        height: 50,
        margin: 40,
    },
    pinStyle: {
        backgroundColor: 'white',
        textAlign: 'center',
        flex: 1,
        marginLeft: 8,
        marginRight: 8,
        shadowColor: 'blue',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 10,
        shadowOpacity: 0,
        fontSize: 30,
        height: 50,
        color: '#068AD5',

    },
    pinTextStyle: {
        textAlign: 'center', color: 'black', fontSize: 15, marginTop: 40,
        fontWeight:'500',
        marginBottom: 20,
        marginRight: 20,
        marginLeft:20
    },
    containerPinStyle: {
        height: 40,

    },
    containerStyle: {
        height: 200,
    },
});

function select(store) {
    return {
        user: store.user,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        verify_register_code: (data) => dispatch(registerActions.verify_register_code(data)),
        resent_code: (data) => dispatch(registerActions.resent_code(data)),
    }
}

export default connect(select, mapDispatchToProps)(ComfirmPINCodeRegisterScreen);