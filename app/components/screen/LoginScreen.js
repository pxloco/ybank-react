import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';
import Button from '../common/ButtonCommon';
import ConstValue from '../consts';
import language from '../lang';
import PasswordInput from '../common/PasswordInput';
import * as loginActions from '../../actions/login';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone_number: '',
            password: '',
            isLoading: false,
            showKeyBoard: false
        }
    }

    componentWillMount() {
        if (this.watchID) navigator.geolocation.clearWatch(this.watchID);
    }

    async onSubmit() {
        this.setState({isLoading: true});
        try {
            await this.props.login_from_server({
                phone_number: this.state.phone_number,
                password: this.state.password
            });
            let actionLogin = null;
            if (this.props.user.status == 3) {
                actionLogin = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'LoginTabScenes',
                        })
                    ]
                });
                this.props.navigation.dispatch(actionLogin);
            } else {
                actionLogin = NavigationActions.reset({
                    index: 1,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'LoginTabScenes',
                        }),
                        NavigationActions.navigate({
                            routeName: this.props.user.is_lender ? 'UpdateProfileLender' : 'UpdateProfileBorrower'
                        })
                    ]
                });

                this.props.navigation.dispatch(actionLogin);
            }

        } catch (message) {
            console.log(message);
            Alert.alert(message);
        } finally {
            this.setState({isLoading: false});
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container} hideNavBar={true}>
                <View style={[styles.layout_input, this.state.showKeyBoard ? {top: -100} : null]}>
                    <Image style={styles.style_logo} source={require('../img/logo_y_bank_hdpi.png')}/>
                    <Text style={styles.title_textview_center}>{language.dangnhaptaikhoan}</Text>

                    <View style={styles.container_input}>
                        <TextInput underlineColorAndroid={'transparent'}
                                   placeholder={language.sodienthoai}
                                   onFocus={() => this.setState({showKeyBoard:true})}
                                   onBlur={() => this.setState({showKeyBoard:false})}
                                   style={styles.text_input}
                                   onChangeText={(phone_number) => this.setState({phone_number})}
                        />
                    </View>
                    <PasswordInput
                        onFocus={() => this.setState({showKeyBoard:true})}
                        onBlur={() => this.setState({showKeyBoard:false})}
                        onChangeText={(password) => this.setState({password})}
                    />
                </View>
                <View style={styles.layout_right}>
                    <TouchableOpacity onPress={
                        () => {
                            navigate('ForgotPasswordStep1')
                        }}>
                        <Text style={styles.textButton}>{language.quenmatkhau}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.layout_input}>
                    <View style={styles.layout_button}>
                        <Button
                            style={{
                                backgroundColor: ConstValue.primaryColorInverse,
                                borderColor: ConstValue.primaryColor
                            }}
                            textStyle={{color: ConstValue.primaryColor, fontSize: 14, fontWeight: 'bold'}}
                            isLoading={ this.state.isLoading }
                            text={language.dangnhap.toUpperCase()}
                            onPress={() => this.onSubmit()}>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button_icon: {
        position: 'absolute',
        right: 3,
        top: 8,
        bottom: 0,
        backgroundColor: 'transparent',
    },
    container_input: {
        width: ConstValue.widthScreen - 40,
        height: 50,
        marginTop: 20,
        borderRadius: 10,
        backgroundColor: "white",
        flexDirection: 'row'
    },
    container: {
        flex: 1,
        backgroundColor: '#068AD5',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    title_textview_center: {
        fontSize: 19,
        textAlign: 'center',
        color: 'white',
        fontWeight: '700',
        marginTop: 15,
        marginBottom: 10
    },
    text_input: {
        fontSize: 16,
        flex: 1,
        textAlign: 'left',
        borderRadius: 6,
        paddingLeft: 8,
        backgroundColor: 'white'
    },
    style_logo: {
        height: 150,
        width: 150,
        marginTop: 20,
        marginBottom: 20
    },
    layout_left: {
        margin: 10,
        alignItems: 'flex-start'
    },
    layout_input: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    layout_button: {
        width: 150,
        height: 50,
    },
    layout_right: {
        alignItems: 'flex-end',
        marginRight: 20,
        marginBottom: 10
    },
    textButton: {
        color: 'white',
        fontSize: 14,
        marginTop: 10,
        marginBottom: 10,
        marginRight: 8,
        fontWeight: '700',
    },
    buttonContainer: {
        backgroundColor: '#2E9298',
        width: 150,
        borderRadius: 20,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 0,
        shadowOpacity: 0,
    },
});

function select(store) {
    return {
        user: store.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login_from_server: (data) => dispatch(loginActions.login_from_server(data))
    }
}

export default connect(select, mapDispatchToProps)(LoginScreen);

