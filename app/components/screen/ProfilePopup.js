import React, {Component} from 'react';

import {
    AppRegistry,
    StyleSheet, Button, Modal,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch,
    TouchableWithoutFeedback,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

import consts from '../consts';

import api from '../../api/api';

import RatingView from '../ui/RatingView';

export default class ProfilePopup extends Component {
    constructor () {
        super();
        this.state = {
            modalVisible: false,
        }
    }

    // region view actions
    requestClose () {
        if (this.props.onRequestClose) {
            this.props.onRequestClose();
        }
    }

    _contact_call () {
        const {user} = this.props;
        if (user) {
            //TODO: show call
        }
    }

    _contact_sms () {
        const {user} = this.props;
        if (user) {
            //TODO: show sms
        }
    }

    _contact_chat () {
        const {user} = this.props;
        if (user && this.props.onStartChat) {
            this.props.onStartChat(user);
        }
    }

    _contact_details () {
        const {user} = this.props;
        if (user) {
            //TODO: show details
        }
    }

    // endregion

    // region component lifecycles
    componentWillReceiveProps (nextProps) {
        const {modalVisible} = nextProps;
        this.setState({modalVisible});
    }

    // endregion

    render () {
        const {modalVisible} = this.state;
        const {user} = this.props;
        return (
            user && <Modal transparent={true} animationType={'fade'} visible={modalVisible}
                           onRequestClose={ () => this.requestClose() }>
                <TouchableOpacity
                    style={styles.container}
                    activeOpacity={1}
                    onPressOut={() => this.requestClose() }
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.inner_container}>
                            { user.avatar ?
                                <Image style={styles.avatar} source={{uri: user.avatar}}/> :
                                <Image style={styles.avatar} source={require('../img/avatar.png')}/>
                            }
                            <Image style={styles.verified} source={require('../img/verified.png')}/>
                            <View style={styles.inner_content_container}>
                                <Text>Thông tin đã xác minh</Text>

                                <View style={styles.space}/>

                                <Text style={styles.name_text}>{user.full_name}</Text>
                                <RatingView value={4} max={5}/>

                                <View style={styles.space}/>
                                <Text style={styles.muted}>Thông tin công ty đang làm việc</Text>
                                <Text>VZOTA</Text>

                                <View style={styles.space}/>
                                <Text style={styles.muted}>Chức danh</Text>
                                <Text>Bảo vệ</Text>

                                <View style={styles.space}/>

                                <View style={styles.btn_line}>
                                    <TouchableHighlight activeOpacity={0.5} underlayColor={'transparent'}
                                                        onPress={() => this._contact_call()}>
                                        <Image
                                            style={styles.button}
                                            source={require('../img/Icon_Contact_Call@3x.png')}
                                        />
                                    </TouchableHighlight>
                                    <TouchableHighlight activeOpacity={0.5} underlayColor={'transparent'}
                                                        onPress={() => this._contact_sms()}>
                                        <Image
                                            style={styles.button}
                                            source={require('../img/Icon_Contact_SMS@3x.png')}
                                        />
                                    </TouchableHighlight>
                                    <TouchableHighlight activeOpacity={0.5} underlayColor={'transparent'}
                                                        onPress={() => this._contact_chat()}>
                                        <Image
                                            style={styles.button}
                                            source={require('../img/Icon_Contact_Chat@3x.png')}
                                        />
                                    </TouchableHighlight>
                                </View>

                                <View style={styles.space}/>

                                <TouchableHighlight style={styles.main_button_container} activeOpacity={0.5}
                                                    underlayColor={consts.primaryColor}
                                                    onPress={() => this._contact_details()}>
                                    <Text style={styles.main_button}>
                                        Thông tin chi tiết
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    inner_container: {
        backgroundColor: 'transparent',
        width: 240,
        alignItems: 'center',
        borderRadius: 5,
    },
    inner_content_container: {
        marginTop: -40,
        paddingTop: 40,
        backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 5,
        zIndex: 100,
    },
    avatar: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 2,
        borderColor: 'white',
        backgroundColor: 'white',
        zIndex: 101,
    },
    verified: {
        marginTop: -10,
        width: 16,
        height: 16,
        borderRadius: 8,
        zIndex: 102,
    },

    name_text: {
        fontSize: 18,
    },

    muted: {
        color: 'gray',
        fontSize: 10,
    },

    space: {
        height: 13,
    },

    btn_line: {
        flexDirection: 'row',
    },

    button: {
        padding: 4,
    },

    main_button_container: {
        flexDirection: 'row',
        backgroundColor: consts.primaryColor,
        padding: 10,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    main_button: {
        flex: 1,
        textAlign: 'center',
        color: consts.primaryColorInverse,
    }
});