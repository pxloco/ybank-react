import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, Switch
} from 'react-native';

import {connect} from 'react-redux';
import {TabNavigator} from 'react-navigation'

export default class PrivateSettingsScreen extends Component {
    static navigationOptions = {
        //header: null
        title: "Cài đặt riêng tư",
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    constructor() {
        super()
        this.state = {
            trueSwitchIsOn: true,
            falseSwitchIsOn: false,
        }
    }

    render() {
        // const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.cai_dat}>
                    <Text style={styles.tieu_de}>Chế độ thông báo trong app</Text>
                    <View style={styles.button_cai_dat}>
                        <Switch
                            onValueChange={(value) => this.setState({falseSwitchIsOn: value})}
                            style={{marginBottom: 0}}
                            onTintColor="#02507E"
                            thumbTintColor="white"
                            tintColor="#02507E"
                            value={this.state.falseSwitchIsOn}/>

                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    cai_dat: {
        marginTop: 20,
        height: 80,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    tieu_de: {
        fontSize: 18,
        marginLeft: 10,
        flex: 5,
    },
    button_cai_dat: {
        alignItems: 'center',
        marginLeft: 10,
        flex: 1,
    }
});