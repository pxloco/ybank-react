import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableOpacity,
    KeyboardAvoidingView,
    TouchableHighlight
} from 'react-native';
import language from '../lang';
import * as registerActions from '../../actions/register';
import {connect} from 'react-redux';
import ConstValue from '../consts';
import Button from '../common/ButtonCommon';
import PasswordInput from '../common/PasswordInput';

class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone_number: '',
            password: '',
            secureText: true,
            isLoading: false
        };
    }

    async createUser() {
        this.setState({
            isLoading: true,
        });
        const is_lender = this.props.navigation.state.params.is_lender;
        try {
            let result = await this.props.create_user({
                phone_number: this.state.phone_number,
                password: this.state.password,
                is_lender: is_lender
            });
            const {navigate} = this.props.navigation;
            navigate('ConfirmPINCodeRegisterScreen');
        } catch (error) {
            Alert.alert(error);
        } finally {
            this.setState({
                isLoading: false,
            });
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior='padding'>
                <View style={styles.layout_input}>
                    <Image style={styles.img_logo} source={require('../img/logo_y_bank_hdpi.png')}/>
                    <Text style={styles.title_textview_center}>{language.taotaikhoanmoi}</Text>
                    <View style={styles.container_input}>
                        <TextInput underlineColorAndroid={'transparent'}
                                   placeholder={language.sodienthoai}
                                   style={styles.text_input}
                                   onChangeText={(phone_number) => this.setState({phone_number})}
                        />
                    </View>

                    <PasswordInput
                        onChangeText={(password) => this.setState({password})}
                    />

                </View>

                <View style={styles.layout_button}>
                    <Button
                        style={{backgroundColor: ConstValue.primaryColorInverse, borderColor: ConstValue.primaryColor}}
                        textStyle={{color: ConstValue.primaryColor, fontSize: 14, fontWeight: 'bold'}}
                        text={'Tiếp tục'}
                        isLoading={ this.state.isLoading }
                        onPress={() => {
                            if (this.state.phone_number == undefined || this.state.phone_number == '' || this.state.password == undefined || this.state.password == '') {
                                Alert.alert("Vui lòng điền số điện thoại và mật khẩu!");
                            } else {
                                if (this.state.password.length < 6 || this.state.password.length > 32) {
                                    Alert.alert("Mật khẩu phải từ 6-32 ký tự!");
                                } else {
                                    this.createUser();
                                }
                            }
                        }}>{language.dangky}</Button>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    img_logo: {
        width: 150,
        height: 150,
    },
    container: {
        backgroundColor: '#068AD5',
        alignItems: 'center',
        // height: 600,
        flex: 1,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    title_textview_center: {
        fontSize: 19,
        textAlign: 'center',
        color: 'white',
        fontWeight: '700',
        marginTop: 20
    },
    container_input: {
        width: ConstValue.widthScreen - 40,
        height: 50,
        marginTop: 20,
        borderRadius: 10,
        backgroundColor: ConstValue.primaryColorInverse,
        flexDirection: 'row'
    },
    text_input: {
        fontSize: 16,
        flex: 1,
        textAlign: 'left',
        borderRadius: 6,
        paddingLeft: 8,
        backgroundColor: 'white'
    },
    button_blue: {
        marginTop: 20,
        height: 50,
        width: 300
    },
    style_logo: {
        height: 150,
        width: 150,
        marginTop: 100,
        marginBottom: 20
    },
    layout_back: {
        margin: 10,
        alignItems: 'flex-start'
    },
    layout_input: {
        alignItems: 'center',
        marginTop: 20,
    },
    back_button: {
        width: 20,
        height: 20,
        margin: 10,
    },
    layout_button: {
        marginTop: 20,
        marginBottom: 10,
        height: 50,
        width: 150,
    },
    password_icon: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    button_icon: {
        position: 'absolute',
        right: 3,
        top: 8,
        bottom: 0,
        backgroundColor: 'transparent',
    }
});

function select(store) {
    return {
        user: store.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        create_user: (data) => dispatch(registerActions.create_user(data))
    }
}

export default connect(select, mapDispatchToProps)(RegisterScreen);
