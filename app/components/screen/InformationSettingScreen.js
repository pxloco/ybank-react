import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, Switch
} from 'react-native';

import {TabNavigator} from 'react-navigation'
import UpdateProfileLender from '../scenes/UpdateProfileLender';
import UpdatePassword from './UpdatePassword';
import * as loginActions from '../../actions/login';
import {connect} from 'react-redux';
import {NavigationActions} from 'react-navigation';
class InformationSettingScreen extends Component {

    constructor() {
        super();
        this.state = {
            trueSwitchIsOn: true,
            falseSwitchIsOn: false,
        }
    }

    sign_out() {
        this.props.sign_out();
        let goToHomeActions = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'LoginTabScenes'
                }),
            ]
        });
        this.props.navigation.dispatch(goToHomeActions);
    }

    request_sign_out() {
        Alert.alert(
            '',
            'Bạn có chắc muốn thoát ứng dụng này',
            [
                {
                    text: 'Không', onPress: () => {
                }
                },
                {text: 'Có', onPress: () => this.sign_out()}
            ],
            {cancelable: false}
        );
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={
                    () => this.props.user.is_lender ? navigate('UpdateProfileLender') : navigate('UpdateProfileBorrower')
                }>
                    <View style={styles.container_thanh_dieu_huong}>
                        <View style={styles.button_ho_tro_online}>
                            <Image style={styles.img_icon_cai_dat}
                                   source={require('../img/Icon_Chinhsuathongtin@3x.png')}/>
                        </View>

                        <View style={styles.thanhdieuhuong}>
                            <Text style={styles.tieu_de}>Chỉnh sửa thông tin</Text>
                        </View>

                        <View style={styles.button_cai_dat}>
                            <Image style={styles.img_icon_chuyen_huong}
                                   source={require('../img/Icon_Caidatthongtin@3x.png')}/>
                        </View>

                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={
                    () => navigate('UpdatePassword')
                }>
                    <View style={styles.container_thanh_dieu_huong}>
                        <View style={styles.button_ho_tro_online}>
                            <Image style={styles.img_icon_cai_dat}
                                   source={require('../img/Icon_Caidattaikhoan@3x.png')}/>

                        </View>
                        <View style={styles.thanhdieuhuong}>
                            <Text style={styles.tieu_de}>Cài đặt tài khoản</Text>
                        </View>
                        <View style={styles.button_cai_dat}>
                            <Image style={styles.img_icon_chuyen_huong}
                                   source={require('../img/Icon_Caidatthongtin@3x.png')}/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={
                    () => navigate('PrivateSettingsScreen')
                }>
                    <View style={styles.container_thanh_dieu_huong}>
                        <View style={styles.button_ho_tro_online}>
                            <Image style={styles.img_icon_cai_dat}
                                   source={require('../img/Icon_Caidatriengtu@3x.png')}/>

                        </View>
                        <View style={styles.thanhdieuhuong}>
                            <Text style={styles.tieu_de}>Cài đặt riêng tư</Text>
                        </View>
                        <View style={styles.button_cai_dat}>
                            <Image style={styles.img_icon_chuyen_huong}
                                   source={require('../img/Icon_Caidatthongtin@3x.png')}/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={
                    () => this.request_sign_out()
                }>
                    <View style={styles.container_thanh_dieu_huong}>
                        <View style={styles.button_ho_tro_online}>
                            <Image style={styles.img_icon_cai_dat} source={require('../img/Icon_Dangxuat@3x.png')}/>
                        </View>
                        <View style={styles.thanhdieuhuong}>
                            <Text style={styles.tieu_de}>Đăng xuất</Text>
                        </View>
                        <View style={styles.button_cai_dat}>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    img_icon_chuyen_huong: {
        width: 15,
        height: 15,
    },
    thanhdieuhuong: {
        flex: 6,
        height: 40,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    button_cai_dat: {
        flex: 1,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button_ho_tro_online: {
        flex: 2,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tieu_de_thanh_dieu_huong: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        marginRight: 30,
    },
    text_ho_tro_online: {
        backgroundColor: 'white',
        color: '#068AD5',
        padding: 5,
        borderRadius: 30,
        marginLeft: 5,
    },
    img_icon_cai_dat: {
        width: 30,
        height: 30,
        marginLeft: 20,
        marginRight: 10,
    },
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    container_thanh_dieu_huong: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: 'white',
        height: 60,
        alignItems: 'center',
    },
});

function select(store) {
    return {
        user: store.user
    }
}

function mapStateToProps(dispatch) {
    return {
        sign_out: () => dispatch(loginActions.sign_out()),
    };
}

export default connect(select, mapStateToProps)(InformationSettingScreen);