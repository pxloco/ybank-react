import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView
} from 'react-native';

import ListViewMessenger from '../../scenes/ListViewMessenger';
import SendSMS from 'react-native-sms';
import call from 'react-native-phone-call';
import Header from '../../common/Header';
import {connect} from 'react-redux';
import UploadImage from '../../common/UploadImage';
import ConstValue from '../../consts';
import Icon from 'react-native-vector-icons/FontAwesome';

var options = {
    title: 'Chọn hình',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

const muc_dich_vay = {
    0: 'Mua nhà',
    1: 'Tiêu dùng',
    2: 'Kinh doanh',
    3: 'Mua ô tô xe máy',
    4: 'Điện máy điện tử',
    5: 'Khác'
};

const nguon_tra_tu = {
    0: 'Lương',
    1: 'Kinh doanh',
    2: 'Bán tài sản',
    3: 'Khác'
};

const chuc_danh = {
    0: 'Hội đồng quản trị',
    1: 'Ban Điều hành',
    2: 'Quản lý',
    3: 'Cầm cố',
    4: 'Khác'
}

class ProfileDetailBorrows extends Component {

    constructor(props) {
        super(props);
    }

    functionSendSMS() {
        SendSMS.send({
            body: '',
            recipients: [this.props.user.phone_number],
            successTypes: ['sent', 'queued']
        }, (completed, cancelled, error) => {
            console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);
        });
    }

    callFunction() {
        const args = {
            number: this.props.user.phone_number,
            prompt: false
        }
        call(args).catch(console.error);
    }

    _renderinfo(image, title, info) {
        return (
            <View style={styles.thong_tin}>
                <View style={styles.can_chieu_ngang}>
                    <Image style={styles.icon} resizeMode={'contain'} source={image}/>
                    <View style={{paddingRight: 10}}>
                        <Text style={styles.tieu_de_thong_tin}>{title}</Text>
                        <Text style={styles.text_thong_tin}>{info}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;
        const {borrower_info} = this.props;
        const settingButton = (<TouchableHighlight style={{marginRight: 16, marginTop: 4}} onPress={() => {
            navigate('InformationSettingScreen');
        }}>
            <Icon size={25}
                  color={'white'}
                  name={"cog"}/>
        </TouchableHighlight>);
        return (
            <View style={styles.container}>
                <Header title={"Trang cá nhân"} rightView={settingButton}/>
                <ScrollView >
                    <View style={styles.thong_tin_ca_nhan}>
                        <View style={styles.avatarContainer}>
                            <Image style={{height: 150, width: ConstValue.widthScreen - 2 * ConstValue.padding}}
                                   source={require('../../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}
                                   resizeMode={'stretch'}
                            />
                            <View style={styles.uploadArea}>
                                <UploadImage
                                    disabled={true}
                                    is_avatar={true}
                                    onImageChange={() => {}}
                                    url={this.props.borrower_info.avatar}
                                    style={styles.avatarArea}/>
                                <Text
                                    style={styles.name}>{borrower_info.full_name ? borrower_info.full_name : 'No Name'}</Text>
                            </View>
                        </View>

                        <View style={styles.nhom_button_lien_lac}>
                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onPress={this.callFunction.bind(this)}>
                                    <Image style={styles.button_lienlac}
                                           source={require('../../img/Icon_Goidien@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>Gọi điện</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onPress={this.functionSendSMS.bind(this)}>
                                    <Image style={styles.button_lienlac} source={require('../../img/Icon_Sms@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>SMS</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onpress={() => {
                                    navigate('ListViewMessenger')
                                }}>
                                    <Image style={styles.button_lienlac}
                                           source={require('../../img/Icon_Chat@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>Nhắn tin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.nhom_thong_tin_lien_he}>
                        {this._renderinfo(require('../../img/Icon_Dia chi_KT3@3x.png'), 'Địa chỉ thường trú / KT3', borrower_info.address)}
                        {this._renderinfo(require('../../img/Icon_So dien thoai@3x.png'), 'Số Điện Thoại', this.props.user.phone_number)}
                        {this._renderinfo(require('../../img/Icon_Chung minh thu@3x.png'), 'Chứng Minh Thư', borrower_info.id_number)}
                        {this._renderinfo(require('../../img/Icon_Gioitinh@3x.png'), 'Giới tính', borrower_info.is_male ? 'Nam' : 'Nữ')}
                        {this._renderinfo(require('../../img/Icon_Tinhtranghonnhan@3x.png'), 'Tình trạng hôn nhân', borrower_info.is_married ? 'Kết hôn' : 'Độc thân')}
                    </View>
                    <View style={styles.nhom_thong_tin_lien_he}>
                        {this._renderinfo(require('../../img/Icon_Mucdichvay@3x.png'), 'Mục đích vay',
                            muc_dich_vay[borrower_info.muc_dich_vay])}

                        {this._renderinfo(require('../../img/Icon_Mucdovay_Thunhaphangthang_Chitieuhangthang_Tongnguontuco@3x.png'),
                            'Mức độ vay', borrower_info.muc_do_vay_2 ? borrower_info.muc_do_vay_2 + ' tỉ' : borrower_info.muc_do_vay_1 + ' triệu')}
                        {this._renderinfo(require('../../img/Icon_Khananghoantra_Gioihankhoanvay@3x.png'), 'Khả năng hoàn trả',
                            nguon_tra_tu[borrower_info.nguon_tra])}
                        {this._renderinfo(require('../../img/Icon_Mucdovay_Thunhaphangthang_Chitieuhangthang_Tongnguontuco@3x.png'),
                            'Số thu nhập hàng tháng', borrower_info.thu_nhap_hang_thang ? borrower_info.thu_nhap_hang_thang : 0 + ' triệu')}
                        {this._renderinfo(require('../../img/Icon_Mucdovay_Thunhaphangthang_Chitieuhangthang_Tongnguontuco@3x.png'),
                            'Số chi tiêu hàng tháng', borrower_info.so_chi_tieu_hang_thangtriệu ? borrower_info.so_chi_tieu_hang_thangtriệu : 0 + ' triệu')}
                        {this._renderinfo(require('../../img/Icon_Congtydanglamviec_Tencongty@3x.png'),
                            'Công ty đang làm việc', borrower_info.cong_ty)}
                        {this._renderinfo(require('../../img/Icon_Chucdanh_Loaihinhkinhdoanh@3x.png'),
                            'Chức danh', chuc_danh[borrower_info.chuc_danh])}
                        {this._renderinfo(require('../../img/Icon_Taisanthechap_Loaihinhcongty@3x.png'),
                            'Tài sản thế chấp', borrower_info.mo_ta_tai_san_the_chap)}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    name: {
        fontSize: 14,
        marginTop: 4,
        fontWeight: '600'
    }
    ,
    avatarArea: {
        width: ConstValue.image.width,
        height: ConstValue.image.height,
        borderRadius: ConstValue.image.height / 2,
        backgroundColor: 'white',
        top: 0,
    },
    uploadArea: {
        position: 'absolute',
        bottom: 0,
        alignItems: 'center'
    },
    avatarContainer: {
        alignItems: 'center',
        height: 210,
        margin: ConstValue.margin,
    },
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    can_chieu_ngang: {
        flexDirection: 'row',
        padding: 5,
    },
    can_chieu_doc: {
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    container_thanh_dieu_huong: {
        flexDirection: 'row',
    },
    button_cai_dat: {
        flex: 1,
        height: 40,
        backgroundColor: '#068AD5',
        justifyContent: 'center',
        alignItems: 'center',
    },
    img_setting: {
        width: 40,
        height: 40,
    },
    thong_tin_ca_nhan: {
        alignItems: 'center',
    },
    hinh_nen_profile: {
        height: 100,
        width: 395,
        resizeMode: 'stretch',
    },
    text_indam_to: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
        marginTop: 5,
        marginBottom: 5,
    },
    text_thong_tin_lien_lac: {
        marginTop: 12,
        fontSize: 12,
        color: '#264C7E',
        textAlign: 'center',
    },

    nhom_button_lien_lac: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        backgroundColor: 'white',
        marginTop: 8
    },
    button_lienlac: {
        width: 20,
        height: 20,
        marginLeft: 50,
        marginRight: 50,
        backgroundColor: 'white'
    },
    icon: {
        width: 15,
        height: 20,
        padding: 5,
        backgroundColor: 'white',
    },
    icon2: {
        width: 20,
        height: 20,
        padding: 5,
    },
    icon3: {
        width: 15,
        height: 25,
        padding: 5,
    },
    icon4: {
        width: 22,
        height: 20,
    },
    icon5: {
        width: 30,
        height: 20,
    },
    nhom_thong_tin_lien_he: {
        marginLeft: 8,
        marginRight: 20,
        padding: 10,
        backgroundColor: 'white',
    },
    tieu_de_thong_tin: {
        color: ConstValue.silverColor,
        fontSize: 12,
        marginBottom: 4,
        marginLeft: 15,
        backgroundColor: 'white'
    },
    text_thong_tin: {
        marginLeft: 15,
        fontSize: 13,
        color: 'black',
        marginTop: 4
    },
    thong_tin: {
        marginTop: 15,
        backgroundColor: 'white'
    }
});

function select(store) {
    return {
        borrower_info: store.borrower_info,
        user: store.user
    }
}

export default connect(select)(ProfileDetailBorrows);