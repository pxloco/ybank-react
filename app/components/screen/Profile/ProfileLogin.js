import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity
} from 'react-native';
import Button from 'apsl-react-native-button';
import ConstValue from '../../consts';
import language from '../../lang';
import ButtonLogin from '../../common/ButtonCommon';

export default class Profile extends Component {
    constructor() {
        super()
        this.funcNavigateRegisterScreenLender = this.funcNavigateRegisterScreenLender.bind(this)
        this.funcNavigateRegisterScreenBorrower = this.funcNavigateRegisterScreenLender.bind(this)
    }

    funcNavigateRegisterScreenBorrower() {
        let {navigate} = this.props.screenProps.navigation;
        navigate('RegisterScreen', {is_lender: false});
    }

    funcNavigateRegisterScreenLender() {
        let {navigate} = this.props.screenProps.navigation;
        navigate('RegisterScreen', {is_lender: true});
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;

        return (
            <View style={styles.container} hideNavBar={false}>
                <View style={styles.navigation_bar}>
                    <View style={styles.navigation_bar_content}>
                        <Text style={styles.title}>{language.trangcanhan}</Text>
                    </View>
                </View>
                <Image style={styles.img_profile_blank}
                       source={require('../../img/gray_icon_navigation_trangcanhan@3x.png')}/>
                <Text style={styles.info_bold}>Guest</Text>

                <Text style={styles.info_grey}>{language.ban_thuoc_doi_tuong_nao.toUpperCase()}</Text>
                <View style={styles.layout_input}>

                    <ButtonLogin
                        style={{backgroundColor: ConstValue.primaryColor, borderColor: ConstValue.primaryColor}}
                        textStyle={{color: 'white', fontSize: 16, fontWeight: 'bold'}}
                        text={language.di_vay.toUpperCase()}
                        onPress={this.funcNavigateRegisterScreenBorrower}
                    />

                    <ButtonLogin
                        style={{backgroundColor: 'white', borderColor: ConstValue.primaryColor}}
                        textStyle={{color: ConstValue.primaryColor, fontSize: 16, fontWeight: 'bold'}}
                        text={language.cho_vay.toUpperCase()}
                        onPress={this.funcNavigateRegisterScreenLender}
                    />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    img_profile_blank: {
        width: 100,
        height: 110,
        marginTop: 20,
    },
    info_bold: {
        margin: 8,
        fontWeight: 'bold',
        color: 'black',
        fontSize: 18,
    },
    info_grey: {
        fontSize: ConstValue.titleText.fontSize + 2,
        fontWeight: '500',
        marginTop: 10,
        color: ConstValue.silverColor
    },
    layout_input: {
        flex: 1,
        flexDirection: 'row'
    },
    layout_button: {
        width: 150,
        height: 50,
        margin: 20,
    },
    navigation_bar: {
        flexDirection: 'row',
        paddingTop: ConstValue.statusBarHeight,
        height: ConstValue.appBarHeight,
        backgroundColor: ConstValue.primaryColor,
    },
    navigation_bar_content: {
        flex: 1,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    icon: {
        width: 25,
        height: 25
    }
});
