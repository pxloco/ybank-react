import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView
} from 'react-native';

import ListViewMessenger from '../../scenes/ListViewMessenger';
import SendSMS from 'react-native-sms';
import call from 'react-native-phone-call';
import Header from '../../common/Header';
import {connect} from 'react-redux';
import UploadImage from '../../common/UploadImage';
import ConstValue from '../../consts';
import Icon from 'react-native-vector-icons/FontAwesome';

const loai_hinh_kinh_doanh = {
    0: 'Cho vay',
    1: 'Cửa hàng cầm cố',
    2: 'Kinh doanh điện máy',
    3: 'Kinh doanh ô tô và xe máy',
    4: 'Chuyên viên tài chính ngân hàng',
    5: 'Khác'
};

const loai_cong_ty = {
    0: 'Cá nhân',
    1: 'Doanh nghiệp tư nhân',
    2: 'Trách nhiệm hữu hạn',
    3: 'Công ty cổ phần',
    4: 'Công ty tài chính ngân hàng',
    5: 'Khác'
}

class ProfileDetailLender extends Component {
    constructor(props) {
        super(props);
    }

    functionSendSMS() {
        SendSMS.send({
            body: '',
            recipients: [this.state.sodienthoai],
            successTypes: ['sent', 'queued']
        }, (completed, cancelled, error) => {
            console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);
        });
    }

    callFunction() {
        const args = {
            number: this.state.sodienthoai, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
        }
        call(args).catch(console.error);
    }

    _renderinfo(image, title, info) {
        return (
            <View style={styles.thong_tin}>
                <View style={styles.can_chieu_ngang}>
                    <Image style={styles.icon} resizeMode={'contain'} source={image}/>
                    <View style={{paddingRight: 10}}>
                        <Text style={styles.tieu_de_thong_tin}>{title}</Text>
                        <Text style={styles.text_thong_tin}>{info}</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;
        const {lender_info} = this.props;
        const settingButton = (<TouchableHighlight style={{marginRight: 16, marginTop: 4}} onPress={() => {
            navigate('InformationSettingScreen');
        }}>
            <Icon size={25}
                  color={'white'}
                  name={"cog"}/>
        </TouchableHighlight>);
        return (
            <View style={styles.container}>
                <Header title={"Trang cá nhân"} rightView={settingButton}/>
                <ScrollView >
                    {/*Thong tin ca nhan*/}
                    <View style={styles.thong_tin_ca_nhan}>
                        <Image style={styles.hinh_nen_profile}
                               source={require('../../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}/>
                        <Image style={styles.avata} source={require('../../img/khoimy.jpg')}/>
                        <Text style={styles.text_indam_to}>Khởi My</Text>

                        {/*Nhom button lien lac*/}
                        <View style={styles.nhom_button_lien_lac}>
                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onPress={this.callFunction.bind(this)}>
                                    <Image style={styles.button_lienlac}
                                           source={require('../../img/Icon_Goidien@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>Gọi điện</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onPress={this.functionSendSMS.bind(this)}>
                                    <Image style={styles.button_lienlac} source={require('../../img/Icon_Sms@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>SMS</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.can_chieu_doc}>
                                <TouchableOpacity onPress={() => {
                                    navigate('ListViewMessenger')
                                }}>
                                    <Image style={styles.button_lienlac} source={require('../../img/Icon_Chat@3x.png')}/>
                                    <Text style={styles.text_thong_tin_lien_lac}>Nhắn tin</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    {/*Nhom thong tin lien he*/}
                    <View style={styles.nhom_thong_tin_lien_he}>
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon} source={require('../../img/Icon_Dia chi_KT3@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Địa điểm</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>29/13 Yên Thế, Phường 9, Xóm 2, Quận Tân Bình,
                                Tp.HCM</Text>
                        </View>
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon3} source={require('../../img/Icon_So dien thoai@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Số điện thoại</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>01210000653245</Text>
                        </View>
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon} source={require('../../img/Icon_Chung minh thu@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Chứng minh thư</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>231425689</Text>
                        </View>
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon2} source={require('../../img/Icon_Gioitinh@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Giới tính</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>Nam</Text>
                        </View>
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon4} source={require('../../img/Icon_Tinhtranghonnhan@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Tình trạng hôn nhân</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>Độc thân</Text>
                        </View>
                    </View>


                    <View style={styles.nhom_thong_tin_lien_he}>
                        {/*Tổng nguồn tự có*/}
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon5}
                                       source={require('../../img/Icon_Mucdovay_Thunhaphangthang_Chitieuhangthang_Tongnguontuco@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Tổng nguồn tự có</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>200 tỉ</Text>
                        </View>

                        {/*Giới hạn khoản vay*/}
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon5}
                                       source={require('../../img/Icon_Mucdovay_Thunhaphangthang_Chitieuhangthang_Tongnguontuco@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Giới hạn khoản vay</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>200 triệu</Text>
                        </View>

                        {/*Loại hình kinh doanh*/}
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon2}
                                       source={require('../../img/Icon_Chucdanh_Loaihinhkinhdoanh@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Loại hình kinh doanh</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>Cá nhân</Text>
                        </View>

                        {/*Loại hình công ty*/}
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon2}
                                       source={require('../../img/Icon_Taisanthechap_Loaihinhcongty@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Loại hình công ty</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>Cá nhân</Text>
                        </View>

                        {/*Tên công ty*/}
                        <View style={styles.thong_tin}>
                            <View style={styles.can_chieu_ngang}>
                                <Image style={styles.icon2}
                                       source={require('../../img/Icon_Congtydanglamviec_Tencongty@3x.png')}/>
                                <Text style={styles.tieu_de_thong_tin}>Tên công ty</Text>
                            </View>
                            <Text style={styles.text_thong_tin}>Công ty ABCD</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    name: {
        fontSize: 14,
        marginTop: 4,
        fontWeight: '600'
    }
    ,
    avatarArea: {
        width: ConstValue.image.width,
        height: ConstValue.image.height,
        borderRadius: ConstValue.image.height / 2,
        backgroundColor: 'white',
        top: 0,
    },
    uploadArea: {
        position: 'absolute',
        bottom: 0,
        alignItems: 'center'
    },
    avatarContainer: {
        alignItems: 'center',
        height: 210,
        margin: ConstValue.margin,
    },
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    can_chieu_ngang: {
        flexDirection: 'row',
        padding: 5,
    },
    can_chieu_doc: {
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    container_thanh_dieu_huong: {
        flexDirection: 'row',
    },
    button_cai_dat: {
        flex: 1,
        height: 40,
        backgroundColor: '#068AD5',
        justifyContent: 'center',
        alignItems: 'center',
    },
    img_setting: {
        width: 40,
        height: 40,
    },
    thong_tin_ca_nhan: {
        alignItems: 'center',
    },
    hinh_nen_profile: {
        height: 100,
        width: 395,
        resizeMode: 'stretch',
    },
    text_indam_to: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
        marginTop: 5,
        marginBottom: 5,
    },
    text_thong_tin_lien_lac: {
        marginTop: 12,
        fontSize: 12,
        color: '#264C7E',
        textAlign: 'center',
    },

    nhom_button_lien_lac: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5,
        backgroundColor: 'white',
        marginTop: 8
    },
    button_lienlac: {
        width: 20,
        height: 20,
        marginLeft: 50,
        marginRight: 50,
        backgroundColor: 'white'
    },
    icon: {
        width: 15,
        height: 20,
        padding: 5,
        backgroundColor: 'white',
    },
    icon2: {
        width: 20,
        height: 20,
        padding: 5,
    },
    icon3: {
        width: 15,
        height: 25,
        padding: 5,
    },
    icon4: {
        width: 22,
        height: 20,
    },
    icon5: {
        width: 30,
        height: 20,
    },
    nhom_thong_tin_lien_he: {
        marginLeft: 8,
        marginRight: 20,
        padding: 10,
        backgroundColor: 'white',
    },
    tieu_de_thong_tin: {
        color: ConstValue.silverColor,
        fontSize: 12,
        marginBottom: 4,
        marginLeft: 15,
        backgroundColor: 'white'
    },
    text_thong_tin: {
        marginLeft: 15,
        fontSize: 13,
        color: 'black',
        marginTop: 4
    },
    thong_tin: {
        marginTop: 15,
        backgroundColor: 'white'
    }
});

function select(store) {
    return {
        lender_info: store.lender_info,
        user: store.user
    }
}

export default connect(select)(ProfileDetailLender);