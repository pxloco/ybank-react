import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch
} from 'react-native';
import Button from 'apsl-react-native-button';
import RegisterScreen from '../RegisterScreen';
import ConstValue from '../../consts'
import language from '../../lang';
import ButtonLogin from '../../common/ButtonCommon';
import HomeHeader from '../../common/HomeHeader';
export default class Profile extends Component {

    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../img/Gray_Icon_Navigation_Map@3x.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    constructor() {
        super()
        this.state = {
            trueSwitchIsOn: true,
            falseSwitchIsOn: false,
        };

        this.funcNavigateRegisterScreenLender = this.funcNavigateRegisterScreenLender.bind(this);
        this.funcNavigateRegisterScreenBorrower = this.funcNavigateRegisterScreenBorrower.bind(this);
    }

    funcNavigateRegisterScreenBorrower() {
        let {navigate} = this.props.screenProps.navigation;
        navigate('RegisterScreen', {is_lender: false});
    }

    funcNavigateRegisterScreenLender() {
        let {navigate} = this.props.screenProps.navigation;
        navigate('RegisterScreen', {is_lender: true});
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;
        return (
            <View style={styles.container}>
                <HomeHeader/>

                {/*Banner*/}
                <View style={styles.banner}>
                    <Image style={styles.img_banner}
                           source={require('../../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}/>
                </View>
                <View style={styles.banner}>
                    <Image style={styles.img_banner}
                           source={require('../../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}/>
                </View>

                {/*Ban thuoc doi tuong nao*/}
                <View style={styles.chon_doi_tuong}>
                    <Text style={styles.info_grey}>{language.ban_thuoc_doi_tuong_nao.toUpperCase()}</Text>
                    <View style={styles.layout_input}>

                        <ButtonLogin
                            style={{backgroundColor: ConstValue.primaryColor, borderColor: ConstValue.primaryColor}}
                            textStyle={{color: 'white', fontSize: 16, fontWeight: 'bold'}}
                            text={language.di_vay.toUpperCase()}
                            onPress={this.funcNavigateRegisterScreenBorrower}
                        />

                        <ButtonLogin
                            style={{backgroundColor: 'white', borderColor: ConstValue.primaryColor}}
                            textStyle={{color: ConstValue.primaryColor, fontSize: 16, fontWeight: 'bold'}}
                            text={language.cho_vay.toUpperCase()}
                            onPress={this.funcNavigateRegisterScreenLender}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    chon_doi_tuong: {
        alignItems: 'center',
        height: 110
    },
    info_grey: {
        fontSize: 16 ,
        fontWeight: '500',
        marginTop: 22,
        marginBottom:8,
        color: ConstValue.silverColor
    },
    layout_input: {
        flex: 1,
        flexDirection: 'row'
    },
    layout_button: {
        width: ConstValue.widthScreen / 2 - 40,
        height: 35,
        margin: 10,
    },
    banner: {
        flex: 1.2,
        backgroundColor: 'white',
        margin: 8,
        marginBottom: 0,
    },
    img_banner: {
        flex: 1,
        width: ConstValue.widthContent
    },

    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    can_chieu_ngang: {
        flexDirection: 'row',
        padding: 5,

    },
    can_chieu_doc: {
        flexDirection: 'column',
    },
    container_thanh_dieu_huong: {
        flexDirection: 'row',
        height: ConstValue.appBarHeight,
        paddingTop: ConstValue.statusBarHeight,
        backgroundColor: ConstValue.primaryColor
    },
    thanhdieuhuong: {
        flex: 6,
        justifyContent: 'center',
    },
    button_cai_dat: {
        flex: 1,
        backgroundColor: '#068AD5',
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 10
    },
    button_ho_tro_online: {
        flex: 3,
        backgroundColor: '#068AD5',
        justifyContent: 'center',
        alignItems: 'center',
    },
    img_setting: {
        width: 40,
        height: 40,
    },
    tieu_de_thanh_dieu_huong: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        marginRight: 30,
    },
    text_ho_tro_online: {
        backgroundColor: 'white',
        color: '#068AD5',
        padding: 5,
        borderRadius: 30,
        marginLeft: 5,
    },
    icon: {
        width: 25,
        height: 25
    }
});