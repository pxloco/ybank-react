// @flow

import React, {Component} from 'react';

import {
    AppRegistry,
    StyleSheet, Button,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView, Switch
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import HomeHeader from '../../common/HomeHeader';
import consts from '../../consts';
import api from '../../../api/api';
import ProfilePopup from '../ProfilePopup';

export default class HomeMap extends Component {

    static navigationOptions = {
        tabBarIcon: ({tintColor}) => (
            <Image
                source={require('../../img/Gray_Icon_Navigation_Map@3x.png')}
                style={[styles.icon, {tintColor: tintColor}]}
            />
        ),
    };

    constructor() {
        super();
        this.state = {
            region: {
                latitude: 10.8184630,
                longitude: 106.6588250,
                latitudeDelta: 0.1,
                longitudeDelta: 0.1,
            },
            locationOrigin: 'register',
            LatLng: {
                latitude: 10.8184630, //'unknown',
                longitude: 106.6588250,//'unknown',
            },
            circle: {
                center: {
                    latitude: 10.8184630,
                    longitude: 106.6588250,
                },
                radius: 100,
            },
            nearby: [],
            showProfile: null,
        }
    }

    // region state mutators
    showProfile(user) {
        this.setState({showProfile: user});
    }

    closeProfilePopup() {
        this.setState({showProfile: null});
    }

    updateLocationOrigin(new_origin) {
        this.setState({locationOrigin: new_origin});
        switch (new_origin) {
            case 'register':

                break;
            case 'gps':

                break;
        }
        this.updateNearbyUsers();
    }

    onRegionChange(region) {
        console.log('region change', region);
        this.setState({region});
    }

    async updateNearbyUsers() {
        let results = await api.get_nearby_user({
            token: this.props.user.token,
            lat: this.state.LatLng.latitude,
            lng: this.state.LatLng.longitude,
            radius: 5000,
        });
        this.setState({nearby: results.results});
    }

    // endregion

    componentDidMount() {
        this.updateNearbyUsers();
    }

    // region renderring functions
    _renderRegionButton(origin, txt) {
        return (
            <TouchableHighlight
                onPress={() => this.updateLocationOrigin(origin)}
                style={[styles.region_select_btn, this.state.locationOrigin == origin && styles.region_select_btn_highlight]}
                underlayColor={consts.primaryColor}>
                <Text
                    style={[styles.region_select_txt, this.state.locationOrigin == origin && styles.region_select_txt_highlight]}>{txt}</Text>
            </TouchableHighlight>
        )
    }

    _renderUserMarker(user, distance) {
        const coordinate = {
            latitude: user.location_lat,
            longitude: user.location_lng,
        };
        return (<MapView.Marker
            key={user.id}
            coordinate={ coordinate }
            image={  user.is_lender ? require('../../img/Icon_User_ChovayCuahangcamdo@3x.png') : require('../../img/Icon_User_Vaytienmat@3x.png')}
            onPress={() => {
                this.showProfile(user);
            }}
        />)
    }

    _renderMeMarker(center, radius) {
        return [
            (<MapView.Circle
                key="user_radius"
                center={center}
                radius={radius}
                fillColor="rgba(176,224,230, 0.5)"
                strokeColor="rgba(0,0,0,0)"
                zIndex={2}
                strokeWidth={0}
            />)
            ,
            (<MapView.Marker
                key="user_marker"
                coordinate={center}
                title="Vị trí của tôi"
                pinColor="rgba(0,0,255)"
                zIndex={2}
            />)
        ];
    }

    render() {
        const {navigate} = this.props.screenProps.navigation;

        const {user} = this.props.user;

        let errors = [];

        if (this.state.locationOrigin == 'register' && user && !user.location_lat && !user.location_lng) {
            errors.push(<Text key='error-loc' style={styles.error}>Bạn chưa cập nhật vị trí đăng ký</Text>);
        }

        return (
            <View style={styles.container}>
                <HomeHeader />
                <View style={styles.region_select_container}>
                    {this._renderRegionButton('register', 'Khu vực đăng ký')}
                    {this._renderRegionButton('gps', 'Khu vực GPS')}
                </View>
                {errors && <View style={styles.error_bar}>{errors}</View>}
                <MapView
                    style={styles.map}
                    region={{...this.state.region}}
                    initialRegion={{...this.state.region}}
                    onRegionChange={ (r) => this.onRegionChange(r) }
                    showsUserLocation={false}
                    followUserLocation={false}
                    zoomEnabled={true}
                >
                    {this.state.circle && this._renderMeMarker(this.state.circle.center, this.state.circle.radius)}
                    {this.state.nearby.map(({user, distance}) => (user && this._renderUserMarker(user, +distance) ))}
                </MapView>
                <ProfilePopup modalVisible={!!this.state.showProfile}
                              user={this.state.showProfile}
                              onStartChat={ (user) => this.showChatForUser(user) }
                              onRequestClose={ () => this.closeProfilePopup() }/>
            </View>
        );
    }

    //endregion

    showChatForUser(user) {
        const {navigate} = this.props.screenProps.navigation;
        this.closeProfilePopup();
        navigate('chat', {username: user.username, user});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: consts.primaryColorInverse,
    },
    error_bar: {
        backgroundColor: 'red',
    },
    error: {
        color: 'white',
    },
    region_select_container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 1,
    },
    region_select_btn: {
        width: 130,
        padding: 3,
        margin: 3,
        borderRadius: 15,
    },
    region_select_btn_highlight: {
        backgroundColor: consts.primaryColor,
    },
    region_select_txt: {
        textAlign: 'center',
        color: consts.primaryColor,
    },
    region_select_txt_highlight: {
        color: consts.primaryColorInverse,
    },
    map: {
        flex: 1,
    },
});