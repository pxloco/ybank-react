import {
    Dimensions,
    Platform
} from 'react-native';
let {height, width} = Dimensions.get('window');
const APPBAR_HEIGHT = Platform.OS == 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS == 'ios' ? 20 : 0;
export default value =  {
    primaryColor: '#068AD5',
    primaryColorInverse: '#FFF',
    silverColor: '#7c7c7c',
    padding: 8,
    margin: 8,

    normalText: {
        fontSize: 12
    },
    titleText: {
        fontSize: 15,
        fontWeight: '600'
    },
    smallText: {
        fontSize: 12,
    },
    h2: {
        fontSize: 14,
        fontWeight: '500'
    },
    titleBullet: {
        fontSize: 20,
        fontWeight: '700',
        width: 25,
        height: 25
    },
    heightScreen: height,
    widthScreen: width,
    widthContent: width - 2* this.padding,
    image: {
        width: 100,
        height: 100,
    },
    inputText: {
        fontSize: 12,
    },
    image_default: '../img/Icon_AnhDaI Dien@3x.png',
    appBarHeight: APPBAR_HEIGHT + STATUSBAR_HEIGHT,
    statusBarHeight: STATUSBAR_HEIGHT
};
