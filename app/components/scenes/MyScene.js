

import React, { Component } from 'react';
import {
  Text, Image,View, Button, StyleSheet
} from 'react-native';

export default class MyScene extends Component {
    //Khai bao bien
    static propTypes = {
      title: React.PropTypes.string,
    };

    //Gan default
    static defaultProps = {
      title: 'Myscene',
    };

    render() {
      return (
        <View>
            <Text>Hi my name is {this.props.title}</Text>
        </View>
        // <View style={styles.container}>
        //   <View style={styles.layout_back}>
        //     <TouchableHighlight onPress={onDangKy}>
        //       <Image  style={styles.style_backbutton} source={require('./button_back_xhdpi.png')} />
        //     </TouchableHighlight>
        //   </View>
        //   <View style={styles.layout_input} >
        //       <Image style= {styles.style_logo}  source={require('./logo_y_bank_hdpi.png')} />
        //       <Text style={styles.title_textview_center}>Tạo tài khoản mới</Text>
        //       <TextInput underlineColorAndroid={'transparent'} placeholder="Số điện thoại" style={styles.text_input} />
        //       <TextInput secureTextEntry={true} underlineColorAndroid={'transparent'} placeholder="Mật khẩu" style={styles.text_input} />
        //       <Button onPress={onDangKy} title={"Đăng ký"} style={styles.button_blue}/>
        //   </View>
        // </View>
      );
    }



}

const onDangKy = () => {
  Alert.alert('Button has been pressed!');
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#397fdf',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  title_textview_center:{
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    fontWeight: "bold",
  },
  text_input:{
    fontSize: 16,
    backgroundColor: "white",
    textAlign: 'left',
    width: 300,
    height: 50,
    borderRadius: 10,
    marginTop: 20
  },
  button_blue:{
    marginTop: 20,
    height: 50,
    width: 300
  },
  style_logo:{
      height: 150,
      width: 150,
      marginTop: 100,
      marginBottom:20
  },
  layout_back:{
    margin: 10,
    alignItems: 'flex-start'
  },
  layout_input:{
    alignItems: 'center'
  }

});
