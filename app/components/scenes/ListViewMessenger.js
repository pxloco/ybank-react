/**
 * Created by pxloc on 09-Jun-17.
 */
import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ListView
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import Button from 'apsl-react-native-button';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
// import SeekBar from 'react-native-android-seekbar';
import BottomNavigation, {Tab} from 'react-native-material-bottom-navigation'
import {TabNavigator} from 'react-navigation'
import {NavigationComponent} from 'react-native-material-bottom-navigation'
import ProfileDetail from '../screen/Profile/ProfileDetailLender';
import UpdateProfileBorrower from './UpdateProfileBorrower';
import UpdateProfileLender from './UpdateProfileLender';

let strings = new LocalizedStrings({
    "vn": {
        trangcanhan: "Trang cá nhân",
    },
    "en-US": {
        vuilongdiensodienthoaivamatkhau: "Please enter your phone number and password!",
    }
})

var ten_nguoi_dung = ['Khởi My', 'Mỹ Tâm', 'Ariana Grande', 'Michael Learns to rock', 'Adell', 'Ember Spirit', 'Crystal', 'Sadino', 'Faker', 'Nevermore'];
var noi_dung_thu_gon = ['Cho mình xin số điện thoại bạn', 'Hẹn bạn 5h chiều nhé.', 'Hẹn bạn quán cà phê no one', 'Hẹn bạn bây giờ nhé.', 'Bạn inbox mình nhé!', 'Đợi mình xíu nhé', 'Cho mình xin số điện thoại bạn', 'Hẹn bạn 5h chiều nhé.', 'Hẹn bạn quán cà phê no one', 'Hẹn bạn bây giờ nhé.', 'Bạn inbox mình nhé!',];
var thoi_gian_online_cach_phut = ["10 phút", "20 phút", "30 phút", "40 phút", "50 phút", "60 phút", "70 phút", "80 phút", "90 phút", "100 phút"];

var AVATA_URLS = [
    require('../img/khoimy.jpg'),
    require('../img/khoimy2.jpg'),
    require('../img/khoimy.jpg'),
    require('../img/khoimy2.jpg'),
    require('../img/khoimy.jpg'),
    require('../img/khoimy2.jpg'),
    require('../img/khoimy.jpg'),
    require('../img/khoimy2.jpg'),
    require('../img/khoimy.jpg'),
    require('../img/khoimy2.jpg'),
];

export default class ListViewMessenger extends Component {
    static navigationOptions = {
        //header: null
        title: 'Nhắn tin',
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    constructor() {
        super();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.genRows({}))
        };
    }

    genRows(pressData: { [key: number]: boolean }): Array<string> {
        var data = [];
        for (var ii = 0; ii < 10; ii++) {
            data.push('Row ' + ii);
        }
        return data;
    }

    renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
        return (
            <View
                key={`${sectionID}-${rowID}`}
                style={{
                    height: adjacentRowHighlighted ? 4 : 1,
                    backgroundColor: adjacentRowHighlighted ? '#068AD5' : '#CCCCCC',
                }}
            />
        );
    }

    renderRow(rowData: string, sectionID: number, rowID: number, highlightRow: (sectionID: number, rowID: number) => void) {
        var imgSource = AVATA_URLS[rowID % 10];

        return (
            <TouchableOpacity onPress={() => {
                highlightRow(sectionID, rowID);
            }}>

                <View style={styles.container_thanh_dieu_huong}>
                    <View style={styles.button_ho_tro_online}>
                        <Image style={styles.avata} source={imgSource}/>
                    </View>
                    <View style={styles.thanhdieuhuong}>
                        <Text style={styles.ten_nguoi_dung}>{ten_nguoi_dung[rowID]}</Text>
                        <Text style={styles.noi_dung_thu_gon}>{noi_dung_thu_gon[rowID]}</Text>
                    </View>
                    <View style={styles.button_cai_dat}>
                        <Image style={styles.img_icon_chuyen_huong} source={require('../img/Online_Circle@3x.png')}/>
                        <Text style={styles.phut_online}>{thoi_gian_online_cach_phut[rowID]}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow}
                    renderSeparator={this.renderSeparator}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ten_nguoi_dung: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
    },
    noi_dung_thu_gon: {
        fontSize: 16,
        color: 'black',
        marginTop: 5,
    },
    phut_online: {
        color: '#068AD5',
        fontSize: 16,
        marginTop: 10,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#F6F6F6',
    },

    thumb: {
        width: 64,
        height: 64,
    },

    text: {
        flex: 1,
        padding: 10
    },
    img_icon_chuyen_huong: {
        width: 15,
        height: 15,
        marginTop: 5,
    },
    thanhdieuhuong: {
        flex: 6,
        height: 40,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    button_cai_dat: {
        flex: 2,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button_ho_tro_online: {
        flex: 3,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tieu_de_thanh_dieu_huong: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        marginRight: 30,
    },
    text_ho_tro_online: {
        backgroundColor: 'white',
        color: '#068AD5',
        padding: 5,
        borderRadius: 30,
        marginLeft: 5,
    },
    avata: {
        width: 80,
        height: 80,
        marginLeft: 20,
        marginRight: 10,
        borderRadius: 45,
        borderWidth: 2,
        borderColor: '#068AD5',
    },
    container_thanh_dieu_huong: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: 'white',
        height: 80,
        alignItems: 'center',
    },

});

AppRegistry.registerComponent('ListViewMessenger', () => ListViewMessenger);