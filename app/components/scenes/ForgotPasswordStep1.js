import React, {Component} from 'React';
import {AppRegistry, StyleSheet, View, Text, TextInput, Alert} from 'react-native';
import Button from 'apsl-react-native-button';
import ForgotPasswordStep3 from './ForgotPasswordStep3';
import LocalizedStrings from 'react-native-localization';

let strings = new LocalizedStrings({
    "vn": {
        chungtoicanxacnhansodienthoai: "Chúng tôi cần xác nhận số điện thoại của bạn. Vui lòng nhập số điện thoại.",
        xacnhansodienthoai: "Xác nhận số điện thoại",
        sodienthoai: "Số điện thoại",
        tieptuc: "Tiếp tục",
    },
    "en-US": {
        chungtoicanxacnhansodienthoai: "We need to confirm your phone number. Please enter the phone number.",
        xacnhansodienthoai: "Confirm phone number",
        sodienthoai: "Phone number",
        tieptuc: "Continue",
    }
})

export default class ForgotPasswordStep1 extends Component {

    static navigationOptions = {
        title: strings.xacnhansodienthoai,
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }


    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.title_xacnhansdt}>{strings.chungtoicanxacnhansodienthoai}</Text>
                <TextInput underlineColorAndroid={'transparent'} placeholder={strings.sodienthoai}
                           style={styles.blue_input} placeholderTextColor="#f8f8fb"></TextInput>
                <View style={styles.btnTiepTuc}>
                    <Button
                        style={{backgroundColor: '#068AD5', borderColor: '#068AD5'}}
                        textStyle={{fontSize: 18, color: 'white', fontWeight: 'bold'}}
                        onPress={() => {
                            navigate('ForgotPasswordStep3')
                        }}>
                        {strings.tieptuc}
                    </Button>
                </View>
            </View>
        );
    }
}

const onDangKy = () => {
    Alert.alert('Tiếp tục');
}


const styles = StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'column',
            backgroundColor: 'white',
            alignItems: 'center',
        },
        blue_input: {
            backgroundColor: '#068AD5',
            width: 300,
            height: 50,
            borderRadius: 10,
            color: 'white',
            fontSize: 16,
            marginLeft: 10,
        },
        title_xacnhansdt: {
            color: 'black',
            fontSize: 18,
            textAlign: 'center',
            marginTop: 20,
            marginBottom: 20,
            marginLeft: 40,
            marginRight: 40,
        },
        btnTiepTuc: {
            width: 150,
            height: 50,
            marginTop: 20,
        }
    }
);

AppRegistry.registerComponent('ForgotPasswordStep1', () => ForgotPasswordStep1)