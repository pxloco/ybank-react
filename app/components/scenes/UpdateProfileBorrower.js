import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView,
} from 'react-native';
import language from '../lang';
import ConstValue from '../consts';
import Input from '../common/Input';
import Legend from '../common/Legend';
import Radio from '../common/Radio';
import Slider from '../common/Slider';
import Button from '../common/Button';
import {connect} from 'react-redux';
import * as UpdateBorrowerInfoAction from '../../actions/info';
import UploadImage from '../common/UploadImage';
import {NavigationActions} from 'react-navigation';

let gioi_tinh_radio_props = [
    {label: language.nam, value: 0},
    {label: language.nu, value: 1},
];

let hon_nhan_radio_props = [
    {label: language.docthan, value: 0},
    {label: language.dacogiadinh, value: 1},
];

let muc_dich_vay_von_radio_props = [
    {label: language.muanha, value: 0},
    {label: language.tieudung, value: 1},
    {label: language.kinhdoanh, value: 2},
    {label: language.muaotoxemay, value: 3},
    {label: language.dienmaydientu, value: 4},
    {label: language.khac, value: 5},
];

let nguon_tra_tu_radio_props = [
    {label: language.luong, value: 0},
    {label: language.kinhdoanh, value: 1},
    {label: language.bantaisan, value: 2},
    {label: language.khac, value: 3},
]

let chuc_danh_radio_props = [
    {label: language.hoidongquantri, value: 0},
    {label: language.bandieuhanh, value: 1},
    {label: language.quanly, value: 2},
    {label: language.nhanvien, value: 3},
    {label: language.khac, value: 4},
]

let tai_san_the_chap_radio_props = [
    {label: language.tinchap, value: 0},
    {label: language.thechap, value: 1},
    {label: language.camco, value: 2},
    {label: language.khac, value: 3},
]


class UpdateProfileBorrower extends Component {

    static navigationOptions = {
        title: language.cap_nhat_thong_tin,
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    constructor(props) {
        super(props);

        this.state = {
            is_loading: false,
            ...this.props.borrower_info
        }
    }

    async submitData() {
        this.setState({is_loading: true});
        let {
            full_name, address, dia_chi_tham_gia_vay, id_number, email, is_male, is_married, muc_dich_vay, muc_do_vay_1,
            muc_do_vay_2, nguon_tra, thu_nhap_hang_thang, so_chi_tieu_hang_thang, cong_ty,
            chuc_danh, tai_san_the_chap, mo_ta_tai_san_the_chap, avatar, chung_tu_giao_dich
        } = this.state;
        try {
            await this.props.update_borrower_info_server({
                api_token: this.props.user.token,
                username: this.props.user.phone_number,
                full_name, address, dia_chi_tham_gia_vay, id_number, email,
                is_male, is_married, muc_dich_vay, muc_do_vay_1,
                muc_do_vay_2, nguon_tra, thu_nhap_hang_thang, so_chi_tieu_hang_thang, cong_ty,
                chuc_danh, tai_san_the_chap, mo_ta_tai_san_the_chap, avatar, chung_tu_giao_dich
            });
            if (this.props.user.status == 2) {
                this.props.update_profile();
                let gotoMainScreen = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'LoginTabScenes'
                        })
                    ]
                });
                this.props.navigation.dispatch(gotoMainScreen);
            } else if (this.props.user.status == 3) {
                Alert.alert('Cập nhật thành công!')
            }
        } catch (message) {
            if (message) {
                Alert.alert(message);
            } else {
                Alert.alert('Vui lòng nhập đầy đủ thông tin');
            }
        } finally {
            this.setState({is_loading: false});
        }
    }

    onPickFile(state) {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
            }
            else {
                let source = {uri: response.uri};
                ImageResizer.createResizedImage(response.uri, 800, 600, 'JPEG', 80)
                    .then((resizedImageUri) => {
                        api.upload_file({uri: resizedImageUri, filename: response.filename}).then(
                            url => {
                                console.log(url);
                                let newState = {};
                                newState[state] = url;
                                this.setState(newState);
                            },
                            message => {
                                console.log(message);
                                if (message)
                                    Alert.alert(message);
                            }
                        );
                    }).catch((err) => {

                });
            }
        });
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.avatarContainer}>
                    <Image style={{height: 150, width: ConstValue.widthScreen - 2 * ConstValue.padding}}
                           source={require('../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}
                           resizeMode={'stretch'}
                    />
                    <View style={styles.uploadArea}>
                        <UploadImage
                            is_avatar={true}
                            onImageChange={(value) => this.setState({avatar: value})}
                            url={this.state.avatar}
                            style={styles.avatarArea}/>
                        <Text style={styles.avatar_description}>{language.nhanvaohinhdethaydoi}</Text>
                    </View>
                </View>

                <View style={styles.form}>
                    <Legend
                        title={language.thong_tin_nguoi_di_vay}
                        number={1}/>
                    <Input
                        label={language.hovaten}
                        value={this.state.full_name}
                        onChangeText={(value) => this.setState({full_name: value})}/>
                    <Input
                        label={language.diachithuongtru}
                        value={this.state.address}
                        onChangeText={(value) => this.setState({address: value})}/>
                    <Input
                        label={language.diachikhuvucthamgiavay}
                        value={this.state.dia_chi_tham_gia_vay}
                        onChangeText={(value) => this.setState({dia_chi_tham_gia_vay: value})}/>
                    <Input
                        label={language.sodienthoai}
                        value={this.props.user.phone_number}
                        editable={false}
                        onChangeText={() => {
                        }}/>
                    <Input
                        label={language.chungminhthu}
                        value={this.state.id_number}
                        onChangeText={(value) => this.setState({id_number: value})}/>
                    <Input
                        label={language.email}
                        value={this.state.email}
                        onChangeText={(value) => this.setState({email: value})}/>

                    <Radio
                        values={gioi_tinh_radio_props}
                        label={language.gioitinh}
                        initial={this.state.is_male ? 0 : 1}
                        onPress={(value) => {
                            this.setState({is_male: !value})
                        }}/>
                    <Radio
                        values={hon_nhan_radio_props}
                        label={language.tinh_trang_hon_nhan}
                        initial={this.state.is_married ? 1 : 0}
                        onPress={(value) => {
                            this.setState({is_married: !!value})
                        }}/>

                    <Legend
                        title={language.nhucauvay}
                        number={2}/>
                    <Radio
                        values={muc_dich_vay_von_radio_props}
                        label={language.mucdichvayvon}
                        initial={this.state.muc_dich_vay}
                        onPress={(value) => this.setState({muc_dich_vay: value})}/>
                    <View style={styles.thong_tin}>
                        <Text style={styles.label}>{language.mucdovay1}</Text>
                        <Slider
                            step={50}
                            value={this.state.muc_do_vay_1}
                            minimumValue={1}
                            maximumValue={990}
                            onValueChange={(value) => this.setState({muc_do_vay_1: value})}
                        />

                    </View>
                    <View style={styles.thong_tin}>
                        <Text style={styles.label}>{language.mucdovay2}</Text>
                        <Slider
                            step={1}
                            value={this.state.muc_do_vay_2}
                            minimumValue={1}
                            maximumValue={50}
                            onValueChange={(value) => this.setState({muc_do_vay_2: value})}/>

                    </View>
                    <Legend
                        title={language.nguontra}
                        number={3}/>
                    <Legend
                        type={2}
                        title={language.nguontratu}
                        number={3.1}/>
                    <Radio
                        values={nguon_tra_tu_radio_props}
                        label={null}
                        initial={this.state.nguon_tra}
                        onPress={(value) => this.setState({nguon_tra: value})}/>
                    <Legend
                        type={2}
                        title={language.sothunhaphangthang}
                        number={3.2}/>
                    <Slider
                        step={1}
                        minimumValue={1}
                        value={this.state.thu_nhap_hang_thang}
                        maximumValue={500}
                        onValueChange={(value) => this.setState({thu_nhap_hang_thang: value})}/>
                    <Legend
                        type={2}
                        title={language.sochitieuhangthang}
                        number={3.3}/>
                    <Slider
                        step={1}
                        minimumValue={1}
                        maximumValue={200}
                        value={this.state.so_chi_tieu_hang_thang}
                        onValueChange={(value) => this.setState({so_chi_tieu_hang_thang: value})}/>
                    <Legend
                        type={2}
                        title={language.congtylamviec}
                        number={3.4}/>
                    <Input
                        label={null}
                        value={this.state.cong_ty}
                        onChangeText={(value) => this.setState({cong_ty: value})}/>
                    <Legend
                        type={2}
                        title={language.chucdanh}
                        number={3.5}/>
                    <Radio
                        values={chuc_danh_radio_props}
                        label={null}
                        initial={this.state.chuc_danh}
                        onPress={(value) => this.setState({chuc_danh: value})}/>

                    <Legend
                        title={language.taisanthechap}
                        number={4}/>
                    <Radio
                        values={tai_san_the_chap_radio_props}
                        label={null}
                        initial={this.state.tai_san_the_chap}
                        onPress={(value) => this.setState({tai_san_the_chap: value})}/>
                    <Input
                        label={language.mota}
                        value={this.state.mo_ta_tai_san_the_chap}
                        onChangeText={(value) => this.setState({mo_ta_tai_san_the_chap: value})}/>

                    <Legend
                        title={language.chungtugiaodich}
                        number={5}/>

                    {/*chung tu giao dich*/}
                    <View style={styles.container_center}>
                        <UploadImage
                            onImageChange={(value) => this.setState({chung_tu_giao_dich: value})}
                            url={this.state.chung_tu_giao_dich}/>
                    </View>

                    <View style={styles.container_center}>
                        <Button
                            isLoading={this.state.is_loading}
                            text={language.capnhat}
                            onPress={() => this.submitData()}/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 200,
    },
    uploadArea: {
        position: 'absolute',
        bottom: 0,
        alignItems: 'center',
    },
    avatarArea: {
        width: ConstValue.image.width,
        height: ConstValue.image.height,
        borderRadius: ConstValue.image.height / 2,
        backgroundColor: 'white',
        paddingTop: -ConstValue.image.width / 2
    },
    avatarImage: {
        width: ConstValue.image.width,
        height: ConstValue.image.height,
        borderRadius: ConstValue.image.height / 2,
        padding: ConstValue.padding,
    },
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    container_center: {
        alignItems: 'center',
    },
    container_left: {
        alignItems: 'flex-start',
    },
    avatar_description: {
        fontSize: ConstValue.normalText.fontSize,
        color: '#c1c1c1',
        marginTop: ConstValue.margin * 2,
    },
    form: {
        padding: ConstValue.padding,
        margin: ConstValue.margin
    },
    label: {
        fontSize: ConstValue.h2.fontSize,
        fontWeight: ConstValue.h2.fontWeight,
        marginBottom: 8
    },
    avatarContainer: {
        alignItems: 'center',
        height: 230,
        margin: ConstValue.margin
    }
});

function select(store) {
    return {
        borrower_info: store.borrower_info,
        user: store.user
    }
}
function mapDispatchToProps(dispatch) {
    return {
        update_borrower_info_server: (data) => dispatch(UpdateBorrowerInfoAction.update_borrower_info_server(data)),
        update_profile: () => dispatch(UpdateBorrowerInfoAction.update_profile())
    }
}

export default connect(select, mapDispatchToProps)(UpdateProfileBorrower);