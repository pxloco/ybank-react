/**
 * Created by pxloc on 13-Jun-17.
 */
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent: {
        backgroundColor: 'white',
        borderRadius: 10,
    },
    modalContentBottom: {
        backgroundColor: 'white',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginLeft: 10,
        marginRight: 10,
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    text_title_modal: {
        textAlign: 'center',
        margin: 10,
        fontSize: 16,
    },
    layout_detail_modal: {
        backgroundColor: 'white',
        borderRadius: 10,
    },
    layout_detail_modal_bottom: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
    },
    layout_info_modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text_close_modal: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        height: 50,
        paddingTop: 15,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        backgroundColor: '#068AD5',
    },
    text_close_modal_bottom: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        height: 50,
        paddingTop: 15,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        backgroundColor: '#068AD5',
    },
    avata: {
        width: 100,
        height: 100,
        borderRadius: 45,
        marginTop: 5,
    },
    avata_modal_bottom: {
        width: 100,
        height: 100,
        borderRadius: 45,
        marginTop: 5,
        marginLeft: 20,
        flex: 2,
    },
    text_modal_button: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
        marginLeft:20,
        marginRight:20,
        marginTop:5,
        marginBottom:5,
    },

    thong_tin_phu: {
        fontSize: 16,
        color: 'black',
    },
    thong_tin_nhan_manh: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
    },
    tieu_de_thong_tin: {
        fontSize: 16,
        marginTop: 10,
    },
    thong_tin_chinh: {
        fontSize: 18,
        color: 'black',
        fontWeight: 'bold',
    },
    layout_contact: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    layout_contact_modal_bottom: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 40,
        marginRight: 10,
        flex: 5,
    },
    button_contact: {
        width: 50,
        height: 50,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 20,
    },
    layout_tin_nhan_thu_gon: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft:20,
        paddingBottom:10,
    },
    thoi_gian_gui_tin_nhan: {
        color: '#068AD5',
        fontSize: 16,
        marginTop: 10,
        flex: 1,
        marginBottom:5,
    },
    thong_tin_ca_nhan: {
        fontSize: 16,
        color: 'black',
        flex: 5,
    },
});