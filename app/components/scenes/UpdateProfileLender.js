import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, Image, TextInput, View, Alert, TouchableHighlight, TouchableOpacity, ScrollView
} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-picker';
import Display from 'react-native-display';
import language from '../lang';
import Input from '../common/Input';
import Legend from '../common/Legend';
import Radio from '../common/Radio';
import Slider from '../common/Slider';
import Button from '../common/Button';
import * as UpdateLenderInfoAction from '../../actions/info';
import {connect} from 'react-redux';
import ConstValue from '../consts'
import UploadImage from '../common/UploadImage';
import {NavigationActions} from 'react-navigation';

const options = {
    title: 'Chọn hình',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};


let gioi_tinh_radio_props = [
    {label: language.nam, value: 0},
    {label: language.nu, value: 1}
];

let hon_nhan_radio_props = [
    {label: language.docthan, value: 0},
    {label: language.dacogiadinh, value: 1},
];

let loai_hinh_kinh_doanh_radio_props = [
    {label: language.chovay, value: 0},
    {label: language.cuahangcamdo, value: 1},
    {label: language.kinhdoanhdienmay, value: 2},
    {label: language.kinhdoanhotovaxemay, value: 3},
    {label: language.chuyenvientaichinhnganhang, value: 4},
    {label: language.khac, value: 5},
];

let loai_cong_ty_radio_props = [
    {label: language.canhan, value: 0},
    {label: language.doanhnghieptunhan, value: 1},
    {label: language.trachnhiemhuuhan, value: 2},
    {label: language.congtycophan, value: 3},
    {label: language.congtytaichinhnganhang, value: 4},
    {label: language.khac, value: 5},
]


class UpdateProfileLender extends Component {

    static navigationOptions = {
        //header: null
        title: language.dangkytaikhoan,
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    constructor (props) {
        super(props);
        console.log(this.props);
        this.state =
            {
                is_loading: false,
                ...this.props.lender_info
            }
    }

    async submitData () {
        this.setState({is_loadding: true});
        let {
            full_name,
            address,
            lend_address,
            id_number,
            email,
            is_male,
            is_married,
            tong_nguon_tu_co,
            gioi_han_khoan_vay,
            loai_hinh_kinh_doanh,
            loai_hinh_cong_ty,
            ten_cong_ty,
            khu_vuc_cho_vay,
            avatar,
            img_hinh_anh_dai_dien,
            img_chung_minh_thu,
            img_giay_phep_kinh_doanh,
            img_hinh_anh_tai_san
        } = this.state;

        try {
            await this.props.update_lender_info_server({
                api_token: this.props.user.token,
                username: this.props.user.phone_number,
                full_name,
                address,
                lend_address,
                id_number,
                email,
                is_male,
                is_married,
                tong_nguon_tu_co,
                gioi_han_khoan_vay,
                loai_hinh_kinh_doanh,
                loai_hinh_cong_ty,
                ten_cong_ty,
                khu_vuc_cho_vay,
                avatar,
                img_hinh_anh_dai_dien,
                img_chung_minh_thu,
                img_giay_phep_kinh_doanh,
                img_hinh_anh_tai_san
            });
            if (this.props.user.status == 2) {
                this.props.update_profile();
                let gotoMainScreen = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'LoginTabScenes'
                        })
                    ]
                });
                this.props.navigation.dispatch(gotoMainScreen);
            } else if (this.props.user.status == 3) {
                Alert.alert('Cập nhật thành công!')
            }
        } catch (message) {
            console.log(message);
            if (message) {
                Alert.alert(message);
            } else {
                Alert.alert('Vui lòng nhập đầy đủ thông tin');
            }
        } finally {
            this.setState({is_loading: false});
        }
    }

    _renderInput (lang_key, state_key) {
        return (
            <Input
                label={lang_key ? language[lang_key] : null}
                value={this.state[state_key]}
                onBlur={(e) => 'text' in e.nativeEvent && this.setState({[state_key]: e.nativeEvent.text})}
                onEndEditing={(e) => 'text' in e.nativeEvent && this.setState({[state_key]: e.nativeEvent.text})}
            />
        );
    }

    render () {
        //const {navigate} = this.props.navigation;
        return (
            <ScrollView style={styles.container}>

                <View style={styles.avatarContainer}>
                    <Image style={{height: 150, width: ConstValue.widthScreen - 2 * ConstValue.padding}}
                           source={require('../img/hinh-nen-powerpoint-ve-cong-nghe-thong-tin-2.jpg')}
                           resizeMode={'stretch'}
                    />
                    <View style={styles.uploadArea}>
                        <UploadImage
                            is_avatar={true}
                            onImageChange={(value) => this.setState({avatar: value})}
                            url={this.state.avatar}
                            style={styles.avatarArea}/>
                        <Text style={styles.avatar_description}>{language.nhanvaohinhdethaydoi}</Text>
                    </View>
                </View>

                <View style={styles.form}>
                    <Legend
                        title={language.thongtinnguoichovay}
                        number={1}
                    />
                    {this._renderInput('hovaten', 'fullname')}
                    {this._renderInput('diachithuongtru', 'address')}
                    {this._renderInput('diachikhuvucthamgiachovay', 'lend_address')}
                    {this._renderInput('sodienthoai', 'phone_number')}
                    {this._renderInput('chungminhthu', 'id_number')}
                    {this._renderInput('email', 'email')}
                    <Radio
                        values={gioi_tinh_radio_props}
                        label={language.gioitinh}
                        initial={this.state.is_male ? 0 : 1}
                        onPress={(value) => {
                            this.setState({is_male: !value})
                        }}/>
                    <Radio
                        values={hon_nhan_radio_props}
                        label={language.tinh_trang_hon_nhan}
                        initial={this.state.is_married ? 1 : 0}
                        onPress={(value) => {
                            this.setState({is_married: !!value})
                        }}/>
                    <Legend
                        title={language.nangluctaichinh}
                        number={2}/>
                    <View style={styles.thong_tin}>
                        <Text style={styles.label}>{language.tongnguontuco}</Text>
                        <Slider
                            step={50}
                            value={this.state.tong_nguon_tu_co}
                            minimumValue={1}
                            maximumValue={990}
                            onValueChange={(value) => this.setState({tong_nguon_tu_co: value})}/>

                    </View>
                    <View style={styles.thong_tin}>
                        <Text style={styles.label}>{language.gioihankhoanvay}</Text>
                        <Slider
                            step={50}
                            value={this.state.gioi_han_khoan_vay}
                            minimumValue={1}
                            maximumValue={990}
                            onValueChange={(value) => this.setState({gioi_han_khoan_vay: value})}/>
                    </View>
                    <Legend
                        title={language.linhvuckinhdoanh}
                        number={3}/>
                    <Legend
                        type={2}
                        title={language.loaihinhkinhdoanh}
                        number={3.1}/>
                    <Radio
                        values={loai_hinh_kinh_doanh_radio_props}
                        label={null}
                        initial={this.state.loai_hinh_kinh_doanh}
                        onPress={(value) => this.setState({loai_hinh_kinh_doanh: value})}/>
                    <Legend
                        type={2}
                        title={language.loaihinhcongty}
                        number={3.2}/>
                    <Radio
                        values={loai_cong_ty_radio_props}
                        label={null}
                        initial={this.state.loai_hinh_cong_ty}
                        onPress={(value) => this.setState({loai_hinh_cong_ty: value})}/>
                    <Legend
                        type={2}
                        title={language.tencongty}
                        number={3.3}/>
                    {this._renderInput(null, 'ten_cong_ty')}
                    <Legend
                        title={language.khuvucchovay}
                        number={4}/>
                    {this._renderInput(null, 'khu_vuc_cho_vay')}
                    <Legend
                        title={language.thongtinxacminh}
                        number={5}/>
                    <Legend
                        type={2}
                        title={language.hinhanhdaidien}
                        number={5.1}/>
                    <View style={styles.container_center}>
                        <View style={styles.container_center}>
                            <UploadImage
                                onImageChange={(value) => this.setState({img_hinh_anh_dai_dien: value})}
                                url={this.state.img_hinh_anh_dai_dien}/>
                        </View>
                    </View>
                    <Legend
                        type={2}
                        title={language.chungminhthu}
                        number={5.2}/>
                    <View style={styles.container_center}>
                        <View style={styles.container_center}>
                            <UploadImage
                                onImageChange={(value) => this.setState({img_chung_minh_thu: value})}
                                url={this.state.img_chung_minh_thu}/>
                        </View>
                    </View>
                    <Legend
                        type={2}
                        title={language.giayphepkinhdoanh}
                        number={5.3}/>
                    <View style={styles.container_center}>
                        <View style={styles.container_center}>
                            <UploadImage
                                onImageChange={(value) => this.setState({img_giay_phep_kinh_doanh: value})}
                                url={this.state.img_giay_phep_kinh_doanh}/>
                        </View>
                    </View>
                    <Legend
                        type={2}
                        title={language.hinhanhtaisan}
                        number={5.4}/>
                    <View style={styles.container_center}>
                        <View style={styles.container_center}>
                            <UploadImage
                                onImageChange={(value) => this.setState({img_hinh_anh_tai_san: value})}
                                url={this.state.img_hinh_anh_tai_san}/>
                        </View>
                    </View>
                </View>

                <View style={styles.container_center}>
                    <Button
                        isLoading={this.state.is_loading}
                        text={language.capnhat}
                        onPress={() => this.submitData()
                        }/>
                </View>
            </ScrollView>
        )
            ;
    }
}

const
    styles = StyleSheet.create({
        avatar_description: {
            fontSize: ConstValue.normalText.fontSize,
            color: '#c1c1c1',
            marginTop: ConstValue.margin * 2,
        },
        image: {
            width: 200,
            height: 200,
        },
        label: {
            fontSize: ConstValue.h2.fontSize,
            fontWeight: ConstValue.h2.fontWeight,
            marginBottom: 8
        },
        avatarContainer: {
            alignItems: 'center',
            height: 230,
            margin: ConstValue.margin
        },
        avatarArea: {
            width: ConstValue.image.width,
            height: ConstValue.image.height,
            borderRadius: ConstValue.image.height / 2,
            backgroundColor: 'white',
            paddingTop: -ConstValue.image.width / 2
        },
        avatarImage: {
            width: ConstValue.image.width,
            height: ConstValue.image.height,
            borderRadius: ConstValue.image.height / 2,
            padding: ConstValue.padding,
        },
        uploadArea: {
            position: 'absolute',
            bottom: 0,
            alignItems: 'center',
        },
        form: {
            padding: ConstValue.padding,
            margin: ConstValue.margin
        },
        img_anh_dai_dien: {
            width: 120,
            height: 120,
            marginTop: 20,
        },
        img_anh_chung_tu: {
            width: 120,
            height: 120,
        },
        container: {
            flex: 1,
            backgroundColor: 'white'
        },
        container_center: {
            alignItems: 'center',
        },
        container_left: {
            alignItems: 'flex-start',
        },
        anh_dai_dien: {
            fontSize: 18,
            color: 'black',
            borderRadius: 45,
        },
        thong_tin_nguoi_cho_vay: {
            margin: 20,
            alignItems: 'flex-start',
        },
        layout_tieu_de_thong_tin: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        so_thu_tu: {
            backgroundColor: '#068AD5',
            borderRadius: 30,
            width: 40,
            height: 40,
            alignItems: 'center',
            paddingLeft: 14,
            paddingTop: 3,
            color: 'white',
            fontSize: 24,
            fontWeight: 'bold',
        },
        so_thu_tu_phu: {
            backgroundColor: '#068AD5',
            borderRadius: 30,
            width: 35,
            height: 35,
            alignItems: 'center',
            paddingLeft: 5,
            paddingTop: 4,
            color: 'white',
            fontSize: 18,
            fontWeight: 'bold',
            marginLeft: 20,
        },

        text_input: {
            fontSize: 16,
            backgroundColor: "white",
            borderWidth: 1.5,
            borderColor: '#068AD5',
            textAlign: 'left',
            width: 350,
            height: 50,
            borderRadius: 10,
            marginTop: 10,
            paddingLeft: 10,
        },
        thong_tin: {
            margin: 10,
            width: 350,
        },
        thong_tin_phu: {
            marginLeft: 20,
            marginTop: 10,
        },
        layout_button: {
            width: 150,
            height: 50,
            margin: 20,
        },

    });

function select (store) {
    return {
        lender_info: store.lender_info,
        user: store.user,
    }
}

function dispatchActionToProps (dispatch) {
    return {
        update_lender_info_server: (data) => dispatch(UpdateLenderInfoAction.update_lender_info_server(data)),
        update_profile: () => dispatch(UpdateLenderInfoAction.update_profile())
    }
}

export default connect(select, dispatchActionToProps)(UpdateProfileLender)