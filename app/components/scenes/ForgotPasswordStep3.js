import React, {Component} from 'React';
import {AppRegistry, StyleSheet, View, Text, TextInput, Alert} from 'react-native';
import Button from 'apsl-react-native-button';
import LocalizedStrings from 'react-native-localization';

let strings = new LocalizedStrings({
    "vn": {
        vuilongnhapmatkhaumoi: "Vui lòng nhập mật khẩu mới của bạn.",
        matkhaumoi: "Mật khẩu mới",
        nhaplaimatkhau: "Nhập lại mật khẩu",
        capnhat: "Cập nhật"
    },
    "en-US": {
        vuilongnhapmatkhaumoi: "Please enter your new password.",
        matkhaumoi: "new password",
        nhaplaimatkhau: "retype password",
        capnhat: "Update"
    }
})

export default class ForgotPasswordStep3 extends Component {
    static navigationOptions = {
        title: "Tạo mật khẩu mới",
        headerTintColor: 'white',
        headerPressColorAndroid: 'blue',
        headerStyle: {backgroundColor: '#068AD5'}
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.title_xacnhanmatkhau}>{strings.vuilongnhapmatkhaumoi}</Text>
                <TextInput secureTextEntry={true} underlineColorAndroid={'transparent'} placeholder={strings.matkhaumoi}
                           style={styles.blue_input} placeholderTextColor="#f8f8fb"></TextInput>
                <TextInput secureTextEntry={true} underlineColorAndroid={'transparent'}
                           placeholder={strings.nhaplaimatkhau}
                           style={styles.blue_input} placeholderTextColor="#f8f8fb"></TextInput>
                <View style={styles.btnTiepTuc}>
                    {/*<Button onPress={onDangKy} title={strings.capnhat}/>*/}
                    <Button
                        style={{backgroundColor: '#068AD5', borderColor: '#068AD5'}}
                        textStyle={{fontSize: 18, color: 'white', fontWeight: 'bold'}}
                        containerStyle={{padding: 0}}
                        onPress={() => {
                            navigate('ConfirmPINCodeRegisterScreen')
                        }}>{strings.capnhat}</Button>
                </View>
            </View>
        );
    }
}

const onDangKy = () => {
    Alert.alert('Cập nhật');
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'column',
            backgroundColor: 'white',
            alignItems: 'center',
        },
        blue_input: {
            backgroundColor: '#068AD5',
            width: 300,
            height: 50,
            borderRadius: 10,
            color: 'white',
            fontSize: 16,
            marginLeft: 10,
            marginTop: 20,
            //placeholderTextColor: 'white',
        },
        title_xacnhanmatkhau: {
            color: 'black',
            fontSize: 18,
            textAlign: 'center',
            marginTop: 20,
            marginBottom: 20,
            marginLeft: 40,
            marginRight: 40,
        },
        btnTiepTuc: {
            width: 150,
            height: 50,
            marginTop: 20,
        }
    }
);
AppRegistry.registerComponent('ForgotPasswordStep3', () => ForgotPasswordStep3)