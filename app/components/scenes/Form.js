import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    TextInput,
    View,
    Alert,
    TouchableHighlight,
    TouchableOpacity,
    Picker,
    StatusBar,
    Navigator,
    Switch, ListView, PermissionsAndroid,
    Platform,
} from 'react-native';
import LocalizedStrings from 'react-native-localization';
import Button from 'apsl-react-native-button';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
// import SeekBar from 'react-native-android-seekbar';
import BottomNavigation, {Tab} from 'react-native-material-bottom-navigation'
import {TabNavigator} from 'react-navigation'
import {NavigationComponent} from 'react-native-material-bottom-navigation'
// import Icon from 'react-native-vector-icons/MaterialIcons'
import {WebView} from 'react-native';
import ProfileDetail from '../screen/Profile/ProfileDetailLender';
import UpdateProfileBorrower from './UpdateProfileBorrower';
import UpdateProfileLender from './UpdateProfileLender';
import Modal from 'react-native-modal';
import SendSMS from 'react-native-sms';
import call from 'react-native-phone-call';
import api from './api';
import StarRating from 'react-native-star-rating';
import MapView, {Marker} from 'react-native-maps';
// import Cookie from 'react-native-cookie';
import {GiftedChat} from 'react-native-gifted-chat';
import Slider from 'react-native-slider'

let strings = new LocalizedStrings({
    "vn": {
        trangcanhan: "Trang cá nhân",
    },
    "en-US": {
        vuilongdiensodienthoaivamatkhau: "Please enter your phone number and password!",
    }
})


var DEFAULT_VALUE = 0.2;

var SliderContainer = React.createClass({
    getInitialState() {
        return {
            value: DEFAULT_VALUE,
        };
    },

    render() {
        var value = this.state.value;

        return (
            <View>
                <View style={styles.titleContainer}>
                    <Text style={styles.caption} numberOfLines={1}>{this.props.caption}</Text>
                    <Text style={styles.value} numberOfLines={1}>{value}</Text>
                </View>
                {this._renderChildren()}
            </View>
        );
    },

    _renderChildren() {
        return React.Children.map(this.props.children, (child) => {
            if (child.type == Slider
                || child.type == ReactNative.Slider) {
                var value = this.state.value;
                return React.cloneElement(child, {
                    value: value,
                    onValueChange: (val) => this.setState({value: val}),
                });
            } else {
                return child;
            }
        });
    },
});


// const args = {
//     number: '9093900003', // String value with the number to call
//     prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
// }
//
// const GEOLOCATION_OPTIONS = {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000};
// const ANCHOR = {x: 0.5, y: 0.5};
//
// const colorOfmyLocationMapMarker = 'blue';
//
// const propTypes = {
//     ...MapView.Marker.propTypes,
//     // override this prop to make it optional
//     coordinate: PropTypes.shape({
//         latitude: PropTypes.number.isRequired,
//         longitude: PropTypes.number.isRequired,
//     }),
//     children: PropTypes.node,
//     geolocationOptions: PropTypes.shape({
//         enableHighAccuracy: PropTypes.bool,
//         timeout: PropTypes.number,
//         maximumAge: PropTypes.number,
//     }),
//     heading: PropTypes.number,
//     enableHack: PropTypes.bool,
// };
//
// const defaultProps = {
//     enableHack: false,
//     geolocationOptions: GEOLOCATION_OPTIONS,
// };

// var CookieManager = require('react-native-cookies');


export default class Profile extends Component {
    getInitialState() {
        return {
            value: 0.2,
        };
    }

    static navigationOptions = {
        tabBarLabel: 'Newsstand',
        tabBarIcon: () => (            < Image size={24} color="white" name="tv"
                                               source={require('../img/Icon_So dien thoai@3x.png')            }/>)
    }

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        ;
        this.onSend = this.onSend.bind(this);
        this.state = {
            messages: [],
            title: '',
            description: '',
            movies: [],
            nameMovie1: '',
            modalVisible: false,
            starCount: 3,
            latitude: null,
            longitude: null,
            error: null,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001,
            name: '',
            camerasFullName: '',
        }
        this.mounted = false;
        this.state = {
            myPosition: null,
        };

    }

    _renderButton = (text, onPress) =>
        (
            <TouchableOpacity
                onPress={onPress}>
                <View
                    style={styles.button }>
                    <Text> {text}
                    </Text>
                </ View >
            </TouchableOpacity >
        )
    ;

    _renderModalContent = () =>
        (
            <View
                style={styles.modalContent
                }>
                <Text> Hello ! </Text>
                {this._renderButton('Close', () => this.setState({visibleModal: null})
                )}
            </View >
        )
    ;
    _renderText = (text) => (
        <Text>{text}</Text>
    )


    watchLocation() {
        // eslint-disable-next-line no-undef
        this.watchID = navigator.geolocation.watchPosition((position) => {
                const myLastPosition = this.state.myPosition;
                const myPosition = position.coords;
                if (!isEqual(myPosition, myLastPosition)) {
                    this.setState({myPosition});
                }
            },
            null, this.props.geolocationOptions
        )
        ;
    }

    onSend(messages = []) {
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            };
        });
    }

    componentDidMount() {
        this.setState({
            messages: [
                {
                    _id: 1,
                    text: 'Hello developer',
                    createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
                    user: {
                        _id: 2,
                        name: 'React Native',
                        avatar: 'https://facebook.github.io/react/img/logo_og.png',
                    },
                },
            ],
        });
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({error: error.message}),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );

        api.getMovies().then((res) => {
            this.setState({
                title: res.title,
                description: res.description,
                movies: res.movies,
                nameMovie1: res.movies[0].title,
            })
        });

        api.getRovers().then((res) => {
            this.setState({
                camerasFullName: res.rovers[0].cameras[0].full_name,
            })
        })


        this.mounted = false;
        if (this.watchID) navigator.geolocation.clearWatch(this.watchID);
    }


    render() {
        // const {navigate} = this.props.navigation;
        console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        console.log("Rovers", this.state.rovers);

        return (
            < View
                style={styles.container}>
                <Text>{this.state.latitude}</Text>
                <Text>{this.state.longitude}</Text>
                <Text>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</Text>
                <SliderContainer caption='<Slider/> with custom style'>
                    <Slider
                        trackStyle={iosStyles.track}
                        thumbStyle={iosStyles.thumb}
                        minimumTrackTintColor='#1073ff'
                        maximumTrackTintColor='#b7b7b7'
                    />
                </SliderContainer>


            </ View >
        )
            ;
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

});

var iosStyles = StyleSheet.create({
    track: {
        height: 2,
        borderRadius: 1,
    },
    thumb: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 2,
        shadowOpacity: 0.35,
    }
});



AppRegistry.registerComponent('MyApp', () => MyApp
)
;