import React, {Component} from 'react';
import {
    AppRegistry,
    Text,
    View,
    StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import LoginStack from './screen/AppStack';
import chat from '../chat/chat';
import UpdatePassword from "./screen/UpdatePassword";

class YbankApp extends Component {
    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        let {user} = nextProps;
        if (user && user.user) {
            console.log('** user', user);
            chat.initialize(user.token, user.user.id);
        } else {
            chat.initialize(null, -1);
        }
    }

    render() {
        return <LoginStack/>;
    }
}

function select(store) {
    return {
        user: store.user,
    }
}

export default connect(select)(YbankApp);