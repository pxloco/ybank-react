const BASE_URL = 'http://23.254.255.114:8082/';
const CREATE_USER = 'api/create_user';
const LOGIN = 'api/login';
const VERIFY_REGISTER_CODE = 'api/verify_register_code';
const RESENT_CODE = 'api/resent_verify_code';
const UPLOAD_FILE = 'api/upload';
const UPDATE_LENDER = 'api/update_lender';
const UPDATE_BORROWER = 'api/update_borrower';
const GET_USER_INFO = 'api/get_user';
const CREATE_TRANSACTION = 'api/create_transaction';
const GET_TRANSACTION = 'api/get_transaction';
const RATING = 'api/create_rating';
const GET_USER_RATING = 'api/get_user_rating';
const LOG = 'api/add_log';
const GET_NEARBY_USER = 'api/nearby';

export default class Api {
    static headers () {
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'dataType': 'json',
        };
    }

    static get (route) {
        return this.xhr(route, null, 'GET');
    }

    static put (route, params) {
        return this.xhr(route, params, 'PUT')
    }

    static post (route, params) {
        return this.xhr(route, params, 'POST')
    }

    static delete (route, params) {
        return this.xhr(route, params, 'DELETE')
    }

    static xhr (route, params, verb) {
        const url = `${BASE_URL}${route}`;
        const options = {method: verb, headers: Api.headers(), ...(params ? {body: JSON.stringify(params)} : {})};

        return fetch(url, options).then(resp => {
            setTimeout( ()=>0 );
            return resp.json();
        });
    }

    static login (data) {
        const params = {
            token: data.token,
            username: data.phone_number,
            password: data.password
        };
        return this.post(LOGIN, params);
    }

    static create_user({phone_number, is_lender, password}) {
        const params = {
            is_lender,
            password,
            username: phone_number,
        };

        return this.post(CREATE_USER, params);
    }

    static verify_register_code(data) {
        const params = {
            username: data.phone_number,
            verified_code: data.verified_code
        }
        return this.post(VERIFY_REGISTER_CODE, params);
    }

    static get_nearby_user(data) {
        let {token, lat, lng, radius = 1000} = data;
        return this.post(GET_NEARBY_USER, {
            lat, lng, radius, api_token: token
        });
    }

    static resent_code(data) {
        let {phone_number} = data;
        return this.post(RESENT_CODE, {
            username: phone_number
        });
    }

    static upload_file(data) {
        let {uri, filename} = data;
        return new Promise((resolve, reject) => {
            const url = `${BASE_URL}${UPLOAD_FILE}`;
            const file = {
                uri,
                name: 'upload.jpg',
                type: filename,
            };
            const body = new FormData();
            body.append('file', file);
            return fetch(url, {
                method: 'POST',
                body
            }).then(response => {
                    response.json().then(
                        result => {
                            if (result.success) {
                                resolve(result.url);
                            } else {
                                resolve(result.message);
                            }
                        }
                    )
                },
                error => {
                    reject('Không kết nối được!')
                }
            )
        });
    }

    static update_lender(data) {
        return this.post(UPDATE_LENDER,{...data});
    }

    static update_borrower(data) {
        return this.post(UPDATE_BORROWER,{...data});
    }

    static get_user_info(data) {

    }

    static log(data) {

    }
}