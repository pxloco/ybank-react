import api from '../api/api';

export function update_borrower_info(data) {
    return {
        type: 'UPDATE_BORROWER_INFO',
        data
    }
}

export function update_lender_info(data) {
    return {
        type: 'UPDATE_LENDER_INFO',
        data
    }
}

export function update_borrower_info_server(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            api.update_borrower({
                api_token: data.api_token,
                username: data.username,
                full_name: data.full_name,
                address: data.address,
                loan_location: data.dia_chi_tham_gia_vay,
                id_num: data.id_number,
                email: data.email,
                is_male: data.is_male,
                is_married: data.is_married,
                loan_demand: data.muc_dich_vay,
                loan_level_1: data.muc_do_vay_1,
                loan_level_2: data.muc_do_vay_2,
                income_from: data.nguon_tra,
                income_per_month: data.thu_nhap_hang_thang,
                outcome_per_month: data.so_chi_tieu_hang_thang,
                working_company: data.cong_ty,
                occupation: data.chuc_danh,
                asset_mortgage: data.tai_san_the_chap,
                asset_mortgage_description: data.mo_ta_tai_san_the_chap,
                avatar: data.avatar,
                transaction_doc: data.chung_tu_giao_dich
            }).then(response => {
                if (response.success == true) {
                    dispatch(update_borrower_info(data));
                    resolve('Cập nhật thành công!');
                } else {
                    reject(response.message);
                }
            }, error => {
                reject('Vui lòng nhập đầy đủ thông tin');
            })
        });
    }
}

export function update_lender_info_server(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            api.update_lender({
                api_token: data.api_token,
                username: data.username,
                full_name: data.full_name,
                avatar: data.avatar,
                address: data.address,
                loan_location: data.lend_address,
                id_num: data.id_number,
                email: data.email,
                is_male: data.is_male,
                is_married: data.is_married,
                loan_capacity: data.tong_nguon_tu_co,
                loan_limit: data.gioi_han_khoan_vay,
                company_type: data.loai_hinh_kinh_doanh,
                company_model: data.loai_hinh_cong_ty,
                company_name: data.ten_cong_ty,
                company_area: data.khu_vuc_cho_vay,
                company_img: data.img_hinh_anh_dai_dien,
                company_id_img: data.img_chung_minh_thu,
                company_license_img: data.img_giay_phep_kinh_doanh,
                company_asset_img: data.img_hinh_anh_tai_san
            }).then(response => {
                if (response.success == true) {
                    dispatch(update_lender_info(data));
                    resolve('Cập nhật thành công!');
                } else {
                    reject(response.message);
                }
            }, error => {
                console.log(error);
                reject('Vui lòng nhập đầy đủ thông tin');
            })
        });
    }
}

export function update_profile(data) {
    return {
        type: 'UPDATE_PROFILE_STATE',
        ...data
    }

}