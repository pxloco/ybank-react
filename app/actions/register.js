import api from '../api/api';
import * as loginActions from './login';
export function create_user(data) {
    return (dispatch) =>
        new Promise(function (resolve, reject) {
            let message = 'Lỗi Kết Nối!';
            api.create_user(data).then(
                (results) => {
                    console.log(results);
                    if (results.success) {
                        dispatch({
                            data: {
                                ...data,
                                status: 1,
                            },
                            type: 'CREATE_USER',
                        });
                        resolve();
                    } else {
                        dispatch({
                            data: {
                                ...data,
                            },
                            type: 'CREATE_USER_FAILURE'
                        });
                        reject(results.message ? results.message : message);
                    }
                },
                (error) => {
                    dispatch({
                        type: 'CREATE_USER_FAILURE'
                    })
                    reject(message);
                }
            );
        })
}

export function verify_register_code(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            let message = 'Lỗi Kết Nối!';
            api.verify_register_code(data).then(
                results => {
                    if (results.success) {
                        dispatch({
                            type: 'VERIFY_TOKEN_SUCCESSFULLY',
                            data: {
                                token: results.api_token,
                                status: 2,
                            }
                        });
                        dispatch(loginActions.login({token:results.api_token,user:results.user}));
                        resolve();
                    } else {
                        dispatch({
                            type: 'VERIFY_TOKEN_FAIL'
                        });
                        reject(results.message ? results.message : message)
                    }
                },
                error => {
                    dispatch({
                        data: {
                            ...data,
                        },
                        type: 'VERIFY_TOKEN_FAIL'
                    });
                    reject(message);
                }
            );
        })
    }
}

export function resent_code(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            let message = 'Lỗi kết nối';
            api.resent_code(data).then(
                results => {
                    resolve(results.message?results.message: message);
                },
                error => {
                    reject(message);
                }
            );
        });
    }
}