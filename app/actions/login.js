import * as types from './types';
import api from '../api/api';
import * as InfoActions from './info';
export function login_from_server(data) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            let message = 'Lỗi Kết Nối!';
            api.login(data).then(response => {
                if (response.success === true) {
                    const {api_token, user} = response;
                    dispatch(login({token: api_token, user}));
                    if (user.status === 3) {
                        if (user.is_lender) {
                            dispatch(InfoActions.update_lender_info({
                                full_name:user.full_name,
                                address:user.address,
                                lend_address: user.loan_location,
                                phone_number: user.username,
                                avatar: user.avatar,
                                id_number: user.id_num,
                                email: user.email,
                                is_male: user.is_male,
                                is_married: user.is_married,
                                tong_nguon_tu_co: user.lender_info.loan_capacity,
                                gioi_han_khoan_vay:user.lender_info.loan_limit,
                                loai_hinh_kinh_doanh: user.lender_info.company_type,
                                loai_hinh_cong_ty: user.lender_info.company_model,
                                ten_cong_ty: user.lender_info.company_name,
                                khu_vuc_cho_vay: user.lender_info.company_area,
                                img_hinh_anh_dai_dien: user.lender_info.company_img,
                                img_chung_minh_thu: user.lender_info.company_id_img,
                                img_giay_phep_kinh_doanh: user.lender_info.company_license_img,
                                img_hinh_anh_tai_san: user.lender_info.company_asset_img,
                            }))
                        } else {
                            dispatch(InfoActions.update_borrower_info({
                                full_name: user.full_name,
                                address: user.address,
                                dia_chi_tham_gia_vay: user.loan_location,
                                phone_number: user.username,
                                avatar: user.avatar,
                                id_number: user.id_num,
                                email: user.email,
                                is_male: user.is_male,
                                is_married: user.is_married,
                                muc_dich_vay: user.borrower_info.loan_demand,
                                muc_do_vay_1: user.borrower_info.loan_level_1,
                                muc_do_vay_2: user.borrower_info.muc_do_vay_2,
                                nguon_tra: user.borrower_info.income_from,
                                thu_nhap_hang_thang: user.borrower_info.income_per_month,
                                so_chi_tieu_hang_thang: user.borrower_info.outcome_per_month,
                                cong_ty: user.borrower_info.working_company,
                                chuc_danh: user.borrower_info.occupation,
                                tai_san_the_chap: user.borrower_info.asset_mortgage,
                                mo_ta_tai_san_the_chap: user.borrower_info.asset_mortgage_description,
                                chung_tu_giao_dich: user.borrower_info.transaction_doc
                            }))
                        }
                    }
                    resolve();
                } else {
                    reject(response.message ? response.message : message);
                }
            }, error => {
                reject(message);
            });
        });
    }
}

export function login(data) {
    return {
        type: 'USER_LOGIN',
        data
    }
}

export function sign_out() {
    return {
        type: 'SIGN_OUT'
    }
}