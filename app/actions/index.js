import * as LoginActions from './login';
import * as GlobalActions from './global';
import * as RegisterActions from './register';
import * as InfoActions from './info';

const ActionCreators = {
    ...LoginActions,
    ...GlobalActions,
    ...RegisterActions,
    ...InfoActions,
};

export default ActionCreators;