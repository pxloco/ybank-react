import config from '../config';

import uuid from 'uuid/v4';

var chat_storage = {};

class ChatSession {
    constructor () {
        this.user_message_updates_map = {};
        this.shouldReconnect = true;
    }

    initialize (api_token, user_id) {
        this.api_token = api_token;
        this.user_id = user_id;
        this.close();
        this.connect();
    }

    onAuthenticated () {

    }

    connect () {
        this.shouldReconnect = true;
        if (this.api_token) {
            this.ws = new WebSocket(config.chat_ws);
            this.ws.onopen = () => this.onOpen();
            this.ws.onmessage = (e) => this.onMessage(e);
            this.ws.onerror = (e) => this.onError(e);
            this.ws.onclose = (e) => this.onClose(e);
        }
    }

    close () {
        this.shouldReconnect = false;
        if (this.ws) {
            this.ws.close();
            this.ws = null;
        }
    }

    onOpen () {
        if (this.ws) {
            this.ws.send(JSON.stringify({
                type: 'auth',
                auth: {
                    token: this.api_token,
                }
            }));
        }
    }

    onMessage (e) {
        let message = JSON.parse(e.data);
        console.log('onMessage', e.data);
        switch (message.type) {
            case 'authenticated':
                break;
            case 'message':
                this.handleMessage(message.message);
                break;
            case 'messages':
                const target_user_ids = (message.message).map((m) => this.handleMessage(m));
                const target_user_ids_unique = target_user_ids.sort().reduce((a, x) => (!a.length || a[a.length - 1] === x) ? a : a.concat([x]), []);
                if ('is_end' in message && message.is_end && 'query_dir' in message && 'for_user' in message) {
                    this.sendCallback(message.for_user, 'cursor_close', message['query_dir']);
                }
                break;
        }
    }

    handleMessage (m) {
        const target_user_id = (m.from_user_id == this.user_id) ? m.to_user_id : m.from_user_id;
        this.sendCallback(target_user_id, 'message', m);
        return target_user_id;
    }

    sendCallback (target_user_id, ...args) {
        if (target_user_id in this.user_message_updates_map) {
            this.user_message_updates_map[target_user_id].forEach((callback) => callback(...args));
        }
    }

    onError (e) {
        console.log('onError', e);
    }

    onClose (e) {
        this.ws = null;
        if (this.shouldReconnect) {
            this.connect();
        }
    }

    loadEarlier (target_user_name, first_message_id) {
        if (this.ws) {
            this.ws.send(JSON.stringify({type: 'messages', target: target_user_name, before: first_message_id}));
        }
    }

    updateMessages (target_user_name, last_message_id) {
        if (this.ws) {
            this.ws.send(JSON.stringify({type: 'messages', target: target_user_name, after: last_message_id}));
        }
    }

    sendMessage (type, message) {
        if (this.ws) {
            this.ws.send(JSON.stringify({type, message}));
        }
    }

    registerMessagesUpdate (user_id, callback) {
        if (!(user_id in this.user_message_updates_map)) {
            this.user_message_updates_map[user_id] = [];
        }
        let self = this;
        this.user_message_updates_map[user_id].push(callback);
        return {
            dispose () {
                if (user_id in self.user_message_updates_map) {
                    const idx = self.user_message_updates_map[user_id].indexOf(callback);
                    self.user_message_updates_map[user_id].splice(idx, 1);
                }
            }
        };
    }
}

let chat = new ChatSession();

export default chat;