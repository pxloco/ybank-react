import YbankApp from './components/YbankApp';
import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';
import {AsyncStorage, AppState, Text, StyleSheet, View} from 'react-native';
import consts from './components/consts';

function configureStore(initialState) {
    const enhancer = compose(
        applyMiddleware(
            thunkMiddleware
        )
    );

    return createStore(reducer, initialState, enhancer);
}
let store = configureStore({});

export default class Root extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isStoreLoading: false,
            store: store
        }
    }

    componentWillMount() {
        let self = this;
        AppState.addEventListener('change', this._handleAppStateChange.bind(this));
        this.setState({isStoreLoading: true});
        AsyncStorage.getItem('completeStore').then((value) => {
            if (value && value.length) {
                let initialStore = JSON.parse(value);
                self.setState({store: configureStore(initialStore)});
            } else {
                self.setState({store: store});
            }
            self.setState({isStoreLoading: false});
        }).catch((error) => {
            self.setState({isStoreLoading: false});
        });
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange.bind(this));
    }

    _handleAppStateChange(currentAppState) {
        let storingValue = JSON.stringify(this.state.store.getState());
        AsyncStorage.setItem('completeStore', storingValue);
    }

    render() {
        if (this.state.isStoreLoading) {
            return (
                <View style={styles.container}>
                    <Text style={styles.content}>Đang cập nhật...</Text>
                </View>
            );
        } else {
            return (
                <Provider store={this.state.store}>
                    <YbankApp/>
                </Provider>
            );
        }
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: consts.primaryColor,
    },
    content: {
        color: consts.primaryColorInverse,
    },
});
