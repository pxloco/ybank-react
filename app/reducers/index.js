import {combineReducers} from 'redux';
import GlobalReducer from './global';
import UserReducer from './user';
import BorrowerInfoReducer from './borrower_info';
import LenderInfoReducer from './lender_info';

export default combineReducers({
    global: GlobalReducer,
    user: UserReducer,
    borrower_info: BorrowerInfoReducer,
    lender_info: LenderInfoReducer
});