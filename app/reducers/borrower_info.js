const initialState = {
    full_name: null,
    address: null,
    dia_chi_tham_gia_vay: null,
    phone_number: null,
    id_number:null,
    email:null,
    is_male:true,
    is_married: false,
    muc_dich_vay: 0,
    muc_do_vay_1:0,
    muc_do_vay_2:0,
    nguon_tra:0,
    thu_nhap_hang_thang:0,
    so_chi_tieu_hang_thang:0,
    cong_ty: null,
    chuc_danh: 0,
    tai_san_the_chap: 0,
    mo_ta_tai_san_the_chap: null,
    avatar: null,
    chung_tu_giao_dich: null,
};

export default function borrower_info (state = initialState, action) {
    if (action.type == 'UPDATE_BORROWER_INFO') {
        return {
            ...state,
            ...action.data
        };
    }
    return state;
}