const initialState = {
    is_logged_in: false,
    is_lender: null,
    phone_number: null,
    token: null,
    status: 0,
    attempt: 0,
    request_created: false

};

export default function user (state = initialState, action) {
    if (action.type == 'USER_LOGIN') {
        let {token, user} = {...state, ...action.data};
        return {
            ...state,
            is_lender: user.is_lender,
            phone_number: user.username,
            token,
            user,
            status: user.status,
            is_logged_in: true,
        }
    }
    if (action.type == 'USER_LOGOUT') {
        return {
            ...state,
            is_logged_in: false,
            is_lender: null,
            phone_number: null,
            token: null,
            status: 0
        }
    }

    if (action.type == 'CREATE_USER') {
        console.log(action.data);
        let {phone_number, is_lender, password, status} = action.data;

        return {
            ...state,
            phone_number: phone_number,
            is_lender: is_lender,
            password: password,
            status: status,
            attempt: 0,
            request_created: true
        }
    }

    if (action.type == 'CREATE_USER_FAILURE') {
        return {
            ...initialState
        }
    }

    if (action.type == 'VERIFY_TOKEN_SUCCESSFULLY') {
        let {token} = action.data;

        return {
            ...state,
            is_logged_in: true,
            token: token,
            status: 2
        }
    }

    if (action.type == 'VERIFY_TOKEN_FAIL') {
        return {
            ...state,
            attempt: state.attempt + 1,
        }
    }

    if (action.type == 'SIGN_OUT') {
        return {
            ...initialState
        };
    }

    if (action.type == 'UPDATE_PROFILE_STATE') {
        return {
            ...state,
            status: 3,
        }
    }

    return state;
}