const initialState = {
    full_name: '',
    address: '',
    lend_address: '',
    id_number:'',
    email:'',
    is_male:true,
    is_married: false,
    tong_nguon_tu_co:0,
    gioi_han_khoan_vay:0,
    loai_hinh_kinh_doanh: 0,
    loai_hinh_cong_ty: 0,
    ten_cong_ty: '',
    khu_vuc_cho_vay: '',
    avatar: '',
    img_hinh_anh_dai_dien: '',
    img_chung_minh_thu: '',
    img_giay_phep_kinh_doanh: '',
    img_hinh_anh_tai_san: '',
};

export default function lender_info (state = initialState, action) {
    if (action.type == 'UPDATE_LENDER_INFO') {
        console.log(action);
        return {
            ...state,
            ...action.data
        };
    }
    return state;
}