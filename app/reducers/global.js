const initialState = {
    topSwitchValue: false,
};

export default function global (state = initialState, action) {
    if (action.type == 'TOGGLE') {
        let {topSwitchValue} = action.data;
        return {
            ...state,
            topSwitchValue: !!topSwitchValue,
        };
    }
    return state;
}